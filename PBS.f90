!_______________________________________________________________________________________________________!
!around 29/8/17
!combined Nic, Nend, Nstar loops
!conditions replaced with abs(k-a*H) .le. 10^-16, instead of k .le. a*H and exit
!editing to make phi evolution adaptive in N to not miss features 
!editing to make R evolution adaptive in N to not miss features 
!to be tested
!30/8/17 : being tested
! returns segmentation fault - invalid memory reference in backqties
! problem with backqty calc. in phi evolution - prob. requires finer step size or lesser demand on precision
!1/9/17 : background works fine - indices of qties corrected - step size and precision for adaptive tuned
! have to check/fine-tune params (esp. phi_0) to adjust punctuation
!7/9/17 
!RK45 adaptive implemented for background
! modes entry and exit calc. malfunctions & takes time - to be checked - maybe use root finder like bisection method
! to do power spec.
!8/9/17
!modes entry & exit taken as first .le. condition ;
!normal rk4 used for perturbation evolutions - check for error estimates
!both spectra are of expected shapes and magnitudes but k ~ 1E-1 give infinities; P_t > P_s - check
! time ~ 20 min; 
! 16/10/17
! trying to optimize time; parallelizing k-loop
! returns high Ps(k); around 10^-2
! verify the scopes (private/shared) of variables - esp. scalarpower, tensorpower and background
!30/10/17
! edited all arrays to column major from row major notation;
! now second index => for labeling/varies across efolds, first index => stores elements / derivatives / step-size / e-fold etc.
! consistent with PH module - all are single dimension arrays
! consistent with params module - no arrays
! consistent with rk_45 module - no arrays
! with adaptive back-evol (only once at the start) and serial k-evol, time taken ~ 1min 50sec
! 2/11/17
! structures defined - BG and Pert in Params.f90 - & passed appropriately during RK calls
! functions characterizing evolution of BG and Pert moved to PH.f90
! single Nic,Nshs,Nstar finder loop
! Power spectra reproduced - time taken ~ 32 sec
! improve Nic,Nshs,Nstar finder to select more k's- make it less stringent maybe ?
! to look into optimal passing of elements of BG/Pert to increase speed
! 3/11/17
! turns out only about 900 k's were selected for calc. of spectra - Nic & Nshs condns - changed back to 'exit' when crossed
! crucially, the shooting of spectra to very high powers as k ~ 1 is rectified - by reducing optimal step-size h ~ 10^-4 in Rk45.f90
! 9/11/17
! corrected (multiplied) a factor of 2.0d0 in the expression of TensorPower - check with other versions of this code - ref. review by L.S.
! 20/4/18
! Nshs for all modes was set as Nend (??arbitrary??), so that deviations from super-Hubble freezing of Rk, during USR, is taken into account.
! 3/3/19
! Nic, Nstar, Nshs conditions are set using z"/z instead of aH for all models; cutoff was redefined in terms of z"/z
! 01/07/19
! separate arrays for e-folds (Field1%efold) and e-fold step size (Field1%estep) were introduced.
! previously they were part of phi and phin: phi(1,i) - efolds, phiN(1,i) - efold step size.
! effect of change checked for KDI and RIPI - PPS and PBS match up to 1e-3% and 1e-1% respectively.
! runtime remains the same.
! 25/08/20
! change in RK calls - separate  arguments for initial and final values of the func solved.
! Corrected the mistake in the second derivative of Dalianis alpha attractor (modified) model.
!_____________________________________________________________________________________________________________________________________________!

PROGRAM PBS

Use Params
Use HV
Use RK_45

Implicit None

Integer :: i, j, j1, j2, j3, jj1, jj2, jj3, ip, jstart, jend, NOOS, iboi, ieoi
Integer :: thrid, num_threads, chunk, OMP_GET_THREAD_NUM, OMP_GET_NUM_THREADS

Type(BG) :: Field1

Complex*16 :: p, pd, Ak, Bk, R_an
Double Precision :: ep, e, s, y, eps, eps1, eps2, eps2N, eps3, Nboi, Neoi
Double Precision :: eta, kappa_trial, berr, maxerr
Double Precision :: k1,k2,k3, ks, kl, ki!, Np
Double Precision :: time1, time2, dummy, dummy2, dlogk
Type(Pert) :: ACurv1, ACurv2, ACurv3

Double precision :: ScalarPower(NOSD), TensorPower(NOSD), moderror(NOSD), modexit(NOSD)

Complex*16, dimension(12,NOS2D) :: componen_RRR
Complex*16, dimension(NOSD) :: Sum_GRRR
Double Precision, dimension(2,NOSD) :: fNL

Complex*16, dimension(NOS2D) :: Sum_GRRR_2D
Double Precision, dimension(NOS2D) :: fNL_2D

Logical :: check
!----------------------------------------------------------------------------------------------------------------------------------------------
!background evolution
!----------------------------------------------------------------------------------------------------------------------------------------------

!NOOS = Nef*Nadap*NOS
NOOS = NOS*Nadap									! testing

allocate(Field1%efold(NOOS))
allocate(Field1%estep(NOOS))				
allocate(Field1%phi(NOOS))
allocate(Field1%phiN(NOOS))				
allocate(Field1%Pot(3,NOOS))				
allocate(Field1%H(2,NOOS))
allocate(Field1%z(3,NOOS))
allocate(Field1%phiNN(NOOS))
allocate(Field1%phiNNN(NOOS))
allocate(Field1%a(NOOS))

call cpu_time(time1)

Field1%paramets(1)=paramet1
Field1%paramets(2)=paramet2

	s = DBLE(Nf-Ni)/DBLE(NOS)						! step size ~ 1/NOS not NOOS

Field1%efold(1) = DBLE(Ni)
Field1%phi(1) = phi_i
Field1%estep = s
Field1%phiN(1) = phiN_i
Field1%Pot(:,1) = V(Field1%phi(1),Field1%paramets)				! feeding the paramets to potential calc.; actually depends on the kind of parameter modelling the background
Field1%phiNN(1) = -((3-phiN_i**2/2)*phiN_i + Field1%Pot(2,1)*(3-Field1%phiN(1)**2/2)/Field1%Pot(1,1))
Field1%H(:,1) = Hfunc(Field1%Pot(:,1),Field1%phi(1),Field1%phiN(1),Field1%phiNN(1))						! H(:,1) is as effective as H(1:2,1)
Field1%phiNNN(1) = -(3.0*Field1%phiNN(1)*(1-Field1%phiN(1)**2/2) + Field1%Pot(2,1)*(Field1%phiN(1)/Field1%H(1,1))**2 &
			& + Field1%Pot(3,1)*Field1%phiN(1)/Field1%H(1,1)**2)
Field1%z(:,1) = zfunc(DBLE(Ni),Field1%phiN(1),Field1%phiNN(1),Field1%phiNNN(1),Field1%H(:,1))


!	Call rk45(DBLE(Ni),DBLE(Nf),s,NOOS,p,pd,f1,f2,backqties,Field1)	! solving background
	Call back_solver(NOOS, Field1, ACurv1, dummy)
	print*
	print*, "Max. error = ", dummy
!----------------------------------------------------------------------------------------------------------------------------------------------
do i=1, Field1%NOOOS/2, 1						! checking for start of inflation till (1/2) of total no. of points
!---------------------------------
	dummy = Field1%phiN(i)*Field1%phiN(i)/2

if (atype .and. (.not. btype)) then
	dummy2 = 3.0d0

else if (btype .and. (.not. atype)) then
	dummy2 = 1.0d0
else
	print*
	print*, "specify epsilon_1i value ! Stopping !"
	STOP
end if
	if(dummy .le. dummy2) then						! where eps_1 drops below 3
!---------------------------------
!	eps1 = Field1%phiN(i)*Field1%phiN(i)/2
!	eps2 = 2.0d0*Field1%phiNN(i)/Field1%phiN(i)
!	eps3 = (Field1%phiNNN(i)/Field1%phiNN(i) - Field1%phiNN(i)/Field1%phiN(i))

!	dummy = eps2-2.0d0*eps1
!	dummy2 = eps2-eps3
!	dummy2 = eps2-eps3							!! testing !!
										! onset of attractor
!	if((abs(dummy) .le. 1.0d-4 .and. abs(dummy2) .le. 1.0d-4)) then				! where eps2 ~ 2*eps1 && eps_3 ~ eps_2
!	if(((eps2+eps3 .eq. 2.0d0*eps2) .and. (eps2 .eq. 2.0d0*eps1))) then			! where eps2 ~ 2*eps1 && eps_3 ~ eps_2
!---------------------------------
!	dummy = Field1%efold(i)
!	if(dummy .ge. 5.0d0) then						! where N = 5
!---------------------------------
		iboi = i
		Nboi = Field1%efold(i)
		exit
	else 
		iboi = 200							! default : Nic~0
		Nboi = Field1%efold(iboi)
	end if
end do

print*
print*, "Nstart = ", Nboi, "eps1(Nstart)=", (Field1%phiN(iboi)**2)/2
!----------------------------------------------------------------------------------------------------------------------------------------------
!do i= Field1%NOOOS, 1, -1						! testing - down counting of e-folds
dummy = 3*Field1%NOOOS/4
do i=INT(dummy), Field1%NOOOS, 1						! checking for end of inflation after (3/4)th of total no. of points

	dummy2 = Field1%phiN(i)*Field1%phiN(i)/2.0d0
	if(dummy2 .gt. 1.0d0) then							! where eps_1 shoots above 1
!	if(dummy2 .le. 1.0d0) then							! testing - where eps_1 drops below 1 when counted from the end
!	if(Field1%efold(i) .gt. 64.0d0) then							! testing - where eps_1 drops below 1 when counted from the end
		ieoi = i
		Neoi = Field1%efold(i)
		exit
	else 
		ieoi = Field1%NOOOS
		Neoi = Field1%efold(ieoi)
	end if
end do

print*, "Nend = ", Neoi
print*, "N_inflation = ", Neoi-Nboi
!----------------------------------------------------------------------------------------------------------------------------------------------
if (.not. aiset) then
ep = Neoi - Np
do i=iboi, ieoi, 1
	dummy = dexp(Field1%efold(i))
	if (Field1%efold(i).ge.ep) then
		ai = kp/(dummy*Field1%H(1,i))
		exit
	end if
end do
end if
!----------------------------------------------------------------------------------------------------------------------------------------------
if (aiset) then								! for models that set ai and calc. Np
do i=iboi, ieoi, 1

	dummy = (ai*dexp(Field1%efold(i))*Field1%H(1,i)) - kp
	if (dummy .ge. 0.0d0) then
		dummy2 = Field1%efold(i)
		exit
	end if
end do
!Np = dummy2
print*, "Npivot = ", Np
end if
!----------------------------------------------------------------------------------------------------------------------------------------------
do i=1, Field1%NOOOS, 1

	dummy = dexp(Field1%efold(i))
	Field1%a(i) = ai*dummy
	Field1%z(:,i)=zfunc(Field1%efold(i),Field1%phiN(i),Field1%phiNN(i),Field1%phiNNN(i),Field1%H(:,i))			! computing z(N), z_N(N), z"/z
end do

print*,"sqrt(z''/z(Nstart)) at the start =", sqrt(abs(Field1%z(3,iboi)))
!----------------------------------------------------------------------------------------------------------------------------------------------
print*
print*, "total points available:", NOOS, "total used :", Field1%NOOOS
print*, "background done"
print*, "ai = ", ai
!----------------------------------------------------------------------------------------------------------------------------------------------
!printing background
!----------------------------------------------------------------------------------------------------------------------------------------------
	Open(unit = 1, file="back_evol.txt")
	write(1,*) "#	i	N	phi	eps_1	V	H	z	zN	z''/z	phiN	eps2	eps2N	eps3	phiNN	eta"

	write(1,*) "# phi_0 =", phi_0
!	write(1,*) "c1 =", c1
!	write(1,*) "c2 =", c2
!	write(1,*) "c3 =", c3
!	write(1,*) "c4 =", c4

!eta = 0.0d0
do i=1, Field1%NOOOS, 10

	eps1 = Field1%phiN(i)*Field1%phiN(i)/2

	eps2 = 2.0d0*Field1%phiNN(i)/Field1%phiN(i)		

	eps2N = 2.0d0*(Field1%phiNNN(i)/Field1%phiN(i) - (Field1%phiNN(i)/Field1%phiN(i))*(Field1%phiNN(i)/Field1%phiN(i)))

	eps3 = (Field1%phiNNN(i)/Field1%phiNN(i) - Field1%phiNN(i)/Field1%phiN(i))

!	eta = eta + Field1%estep(i)/(ai*dexp(Field1%efold(i))*Field1%H(1,i))				! computing eta(N)

	dummy = -Field1%Pot(2,i)/(3.0d0*Field1%H(1,i)*Field1%H(1,i))					! SR approx. of dphi/dN

	write(1,*) i, Field1%efold(i), Field1%phi(i), eps1, Field1%Pot(1,i), Field1%H(1,i), &
		 	Field1%z(1,i), Field1%z(2,i), Field1%z(3,i), Field1%phiN(i), &
			eps2, eps2N, eps3, Field1%phiNN(i), Field1%Pot(2,i), Field1%Pot(3,i), dummy
end do
	write(1,*)	
print*
print*, "background written in back_evol.txt"
	Close(1)
!----------------------------------------------------------------------------------------------------------------------------------------------
!perturbations 
!----------------------------------------------------------------------------------------------------------------------------------------------
if (do_perturb) then
!----------------------------------------------------------------------------------------------------------------------------------------------
!PPS
!----------------------------------------------------------------------------------------------------------------------------------------------
if (do_RR) then

!$OMP PARALLEL NUM_THREADS(no_cores) DEFAULT(SHARED) &
!$OMP& PRIVATE(thrid,y,k1,ACurv1) &
!$OMP& SHARED(Field1) &
!$OMP& SHARED(ScalarPower,TensorPower) &
!$OMP& SHARED(chunk,num_threads)

thrid = OMP_GET_THREAD_NUM()
num_threads = OMP_GET_NUM_THREADS()
chunk = NOSD/num_threads/2
!----------------------------------------------------------------------------------------------------------------------------------------------
allocate(ACurv1%R(Field1%NOOOS))
allocate(ACurv1%RN(Field1%NOOOS))
allocate(ACurv1%RC(Field1%NOOOS))
allocate(ACurv1%RCN(Field1%NOOOS))
allocate(ACurv1%T(Field1%NOOOS))
allocate(ACurv1%TN(Field1%NOOOS))
!----------------------------------------------------------------------------------------------------------------------------------------------
if(thrid.eq.0) then
print*
print*, "number of threads = ", num_threads, " chunks = ", chunk
print*, "calculating PPS..."
end if

!$OMP DO SCHEDULE(DYNAMIC, chunk)
do j=1, NOSD, 1

	y = log10(kmin) + DBLE(j)*(log10(kmax/kmin))/DBLE(NOSD)
	k1 = 10**y
	ACurv1%k = k1

	Call Pert_Solver(Field1,ACurv1,k1,k1)

 ScalarPower(j) = ACurv1%SPS
 TensorPower(j) = ACurv1%TPS
 moderror(j) = ACurv1%Rerr
 modexit(j) = ACurv1%Nstar

end do
!$OMP END DO
!----------------------------------------------------------------------------------------------------------------------------------------------
deallocate(ACurv1%R)
deallocate(ACurv1%RN)
deallocate(ACurv1%RC)
deallocate(ACurv1%RCN)
deallocate(ACurv1%T)
deallocate(ACurv1%TN)
!----------------------------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL
!----------------------------------------------------------------------------------------------------------------------------------------------
end if
!----------------------------------------------------------------------------------------------------------------------------------------------
!PBS
!----------------------------------------------------------------------------------------------------------------------------------------------
if (do_RRR) then

componen_RRR = 0

Sum_GRRR_2D = 0
fNL_2D = 0

Sum_GRRR = 0
fNL = 0

!$OMP PARALLEL NUM_THREADS(no_cores) DEFAULT(SHARED) &
!$OMP& SHARED(Field1) &
!$OMP& PRIVATE(jj1,k1,k2,k3,ks,kl,ACurv1,ACurv2,ACurv3,kappa_trial,dummy) &
!$OMP& SHARED(Sum_GRRR_2D,fNL_2D,Sum_GRRR,fNL) &
!$OMP& SHARED(componen_RRR) &
!$OMP& PRIVATE(thrid,y) &
!$OMP& SHARED(chunk,num_threads)

thrid = OMP_GET_THREAD_NUM()
num_threads = OMP_GET_NUM_THREADS()
chunk = NOSD/num_threads/2

!----------------------------------------------------------------------------------------------------------------------------------------------
allocate(ACurv1%R(Field1%NOOOS))
allocate(ACurv1%RN(Field1%NOOOS))
allocate(ACurv1%RC(Field1%NOOOS))
allocate(ACurv1%RCN(Field1%NOOOS))
allocate(ACurv1%T(Field1%NOOOS))
allocate(ACurv1%TN(Field1%NOOOS))

allocate(ACurv2%R(Field1%NOOOS))
allocate(ACurv2%RN(Field1%NOOOS))
allocate(ACurv2%RC(Field1%NOOOS))
allocate(ACurv2%RCN(Field1%NOOOS))
allocate(ACurv2%T(Field1%NOOOS))
allocate(ACurv2%TN(Field1%NOOOS))

allocate(ACurv3%R(Field1%NOOOS))
allocate(ACurv3%RN(Field1%NOOOS))
allocate(ACurv3%RC(Field1%NOOOS))
allocate(ACurv3%RCN(Field1%NOOOS))
allocate(ACurv3%T(Field1%NOOOS))
allocate(ACurv3%TN(Field1%NOOOS))
!----------------------------------------------------------------------------------------------------------------------------------------------

if(thrid.eq.0) then
print*
print*, "number of threads = ", num_threads, " chunks = ", chunk
print*, "calculating PBS ..."
end if

!----------------------------------------------------------------------------------------------------------------------------------------------
	if(do_kappa) then

		!$OMP DO SCHEDULE(DYNAMIC, chunk)		
		do j1=1, NOSD, 1

			k1 = k_kappa
			y = -4.0d0 + DBLE(j1)*(5.0d0)/DBLE(NOSD)					! finding kappa
			kappa_trial = 10**y

			ACurv1%k = k1
			Call Pert_Solver(Field1,ACurv1,k1,k1)							! equilateral
			Call RRR_integrator(Field1,ACurv1,ACurv1,ACurv1,kappa_trial,componen_RRR(:,j1),Sum_GRRR(j1),fNL(:,j1))	! equilateral
		end do
		!$OMP END DO
	end if
!----------------------------------------------------------------------------------------------------------------------------------------------
	if(do_equil) then

		!$OMP DO SCHEDULE(DYNAMIC, chunk)		
		do j2=1, NOSD, 1

			y = log10(kmin) + DBLE(j2)*(log10(kmax/kmin))/DBLE(NOSD)
			k1 = 10**y

			ACurv1%k = k1
			Call Pert_Solver(Field1,ACurv1,k1,k1)							! equilateral
			Call RRR_integrator(Field1,ACurv1,ACurv1,ACurv1,kappa,componen_RRR(:,j2),Sum_GRRR(j2),fNL(:,j2))	! equilateral
		end do
		!$OMP END DO
	end if
!----------------------------------------------------------------------------------------------------------------------------------------------
	if(do_squeez) then

		!$OMP DO SCHEDULE(DYNAMIC, chunk)		
		do j3=1, NOSD, 1

			y = log10(kmin) + DBLE(j3)*(log10(kmax/kmin))/DBLE(NOSD)
			k1 = 10**y
			k2 = k1

!			k3 = (1.0d-3)*k1
!			k3 = kmin			! squeezed limit
			k3 = ksqueez

			ks = k3
			kl = k1
	
			ACurv1%k = k1
			Call Pert_Solver(Field1,ACurv1,kl,kl)
	
			ACurv2%k = k2
			Call Pert_Solver(Field1,ACurv2,kl,kl)

			ACurv3%k = k3
			Call Pert_Solver(Field1,ACurv3,ks,kl)

			Call RRR_integrator(Field1,ACurv1,ACurv2,ACurv3,kappa,componen_RRR(:,j3),Sum_GRRR(j3),fNL(:,j3))	! testing - print components if required
		end do
		!$OMP END DO
	end if
!----------------------------------------------------------------------------------------------------------------------------------------------
	if(do_2D) then

		k1 = k2D

if (thrid .eq. 0) then
print*, "calculating PBS for 2D plot..."
print*, "mode chosen as k1 = ", k1
end if
		!$OMP DO SCHEDULE(DYNAMIC, chunk)		
		do j2=1, NOSD, 1
!		k2 = 0.00d0*k1 + (1.0d0)*k1*DBLE(j2)/DBLE(NOSD)
		k3 = 0.0d0*k1 + (1.0d0)*k1*DBLE(j2)/DBLE(NOSD)			! testing

			do j3=1, NOSD, 1
!			k3 = 0.0d0*k1 + (1.0d0)*k1*DBLE(j3)/DBLE(NOSD)

			if (k3 .le. k1/2.0d0) then
				k2 = k1-k3 + k3*DBLE(j3)/DBLE(NOSD)		! testing
			else
				k2 = k3 + (k1-k3)*DBLE(j3)/DBLE(NOSD)		! testing
			end if

!----------------------------------------------------------------------------------------------------------------------------------------------
!finding smallest and largest k's of three, to set Nic and Nshs
!----------------------------------------------------------------------------------------------------------------------------------------------
		if (k1 .le. k2) then
			ks = k1
			kl = k2
		else 
			ks = k2
			kl = k1
		end if

		if (k3 .le. ks) then
			ks = k3
		else if (k3 .ge. kl) then
			kl = k3
		else
			continue
		end if
!----------------------------------------------------------------------------------------------------------------------------------------------
!		if (k3 .ge. abs(k2-k1) .and. k3 .ge. k2) then
!		if (k3 .ge. abs(k2-k1)) then

			ACurv1%k = k1
			Call Pert_Solver(Field1,ACurv1,ks,kl)
	
			ACurv2%k = k2
			Call Pert_Solver(Field1,ACurv2,ks,kl)

			ACurv3%k = k3
			Call Pert_Solver(Field1,ACurv3,ks,kl)

			jj1 = (j2-1)*NOSD+j3

!			Call RRR_integrator(Field1,ACurv1,ACurv2,ACurv3,kappa,Sum_GRRR_2D(jj1),fNL_2D(jj1))

			Call RRR_integrator(Field1,ACurv1,ACurv2,ACurv3,kappa,componen_RRR(:,jj1),Sum_GRRR_2D(jj1),fNL_2D(jj1))	! testing - print components if required

!		else
!			continue
!		end if
!----------------------------------------------------------------------------------------------------------------------------------------------
			end do				! j3 loop
!----------------------------------------------------------------------------------------------------------------------------------------------
		end do					! j2 loop
		!$OMP END DO
!----------------------------------------------------------------------------------------------------------------------------------------------
deallocate(ACurv1%R)
deallocate(ACurv1%RN)
deallocate(ACurv1%RC)
deallocate(ACurv1%RCN)
deallocate(ACurv1%T)
deallocate(ACurv1%TN)

deallocate(ACurv2%R)
deallocate(ACurv2%RN)
deallocate(ACurv2%RC)
deallocate(ACurv2%RCN)
deallocate(ACurv2%T)
deallocate(ACurv2%TN)

deallocate(ACurv3%R)
deallocate(ACurv3%RN)
deallocate(ACurv3%RC)
deallocate(ACurv3%RCN)
deallocate(ACurv3%T)
deallocate(ACurv3%TN)
!----------------------------------------------------------------------------------------------------------------------------------------------
	end if
!----------------------------------------------------------------------------------------------------------------------------------------------
!$OMP END PARALLEL
!----------------------------------------------------------------------------------------------------------------------------------------------
end if										! RRR if
!----------------------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------------------------------------
! output to files
!----------------------------------------------------------------------------------------------------------------------------------------------
if (do_perturb) then

if(do_mode_test) then

allocate(ACurv1%R(Field1%NOOOS))
allocate(ACurv1%RN(Field1%NOOOS))
allocate(ACurv1%RC(Field1%NOOOS))
allocate(ACurv1%RCN(Field1%NOOOS))
allocate(ACurv1%Rgr(Field1%NOOOS))
allocate(ACurv1%Rde(Field1%NOOOS))
allocate(ACurv1%T(Field1%NOOOS))
allocate(ACurv1%TN(Field1%NOOOS))
allocate(ACurv1%S(Field1%NOOOS))

	ACurv1%k = k_test
	Call Pert_Solver(Field1,ACurv1,ACurv1%k,ACurv1%k)

	Open(unit = 2, file="Pert_evol.txt")
	write(2,*) "#k = ", ACurv1%k
	write(2,*) "#N	REAL(R(N))	IMG(R(N))	REAL(RN(N))	IMG(RN(N))	REAL(T(N))	IMG(T(N))	REAL(TN(N))	IMG(TN(N))	REAL(S(N))	IMG(S(N))"
	
	print*
	print*, "k for Pert_evol = ", ACurv1%k

!-------------------
	ACurv1%i_s = ACurv1%i_s + 1000			! testing

	Ak = ACurv1%R(ACurv1%i_s)
!	Bk = Field1%a(ACurv1%i_s)*Field1%H(1,ACurv1%i_s)*ACurv1%RN(ACurv1%i_s)*Field1%z(1,ACurv1%i_s)**2

	Bk = ACurv1%RN(ACurv1%i_s)	! testing

	ACurv1%Rgr(i) = 0.0d0
	ACurv1%Rde(i) = 0.0d0
!-------------------

	do i=ACurv1%i_a, ACurv1%i_z, 1

	if (i .ge. ACurv1%i_s) then
		ACurv1%Rgr(i) = Ak				! super-Hubble solutions
!		ACurv1%Rde(i) = ACurv1%Rde(i) + Bk*Field1%estep(i)/(Field1%a(i)*Field1%H(1,i)*Field1%z(1,i)*Field1%z(1,i))

!		dummy = 3.0d0*(Field1%efold(ACurv1%i_s) - e)
!		dummy2 = Field1%efold(ACurv1%i_s)
!		ACurv1%Rde(i) = ACurv1%Rde(i) + Bk*dexp(dummy)*s*(HSRP1(dummy2)/HSRP1(e))*(Field1%H(1,ACurv1%i_s)/Field1%H(1,i))		! testing

		Bk = ACurv1%RN(i)*Field1%a(i)*Field1%H(1,i)*Field1%z(1,i)**2
		ACurv1%Rde(i) = ACurv1%Rde(i) +  ACurv1%RN(i)*s
	else
		ACurv1%Rgr(i) = 0.0d0
		ACurv1%Rde(i) = 0.0d0
	end if

!-------------------calc. of Sk-----------------

	eps1 = ((Field1%phiN(i)*Field1%phiN(i))/2.0d0)
	eps2 = 2.0d0*Field1%phiNN(i)/Field1%phiN(i)		
	dummy = Field1%H(1,i)**4*(eps1*eps2 - 2.0d0*eps1*eps1)**2 - &
		 Field1%Pot(2,i)*Field1%Pot(2,i)*Field1%phiN(i)*Field1%phiN(i)

	ACurv1%S(i) = -ACurv1%RN(i)*Field1%Pot(2,i)*Field1%phiN(i)*eps1/dummy

!-------------------

	write(2,*) Field1%efold(i), ((Acurv1%k)**1.5d0)*DBLE(ACurv1%R(i)), ((Acurv1%k)**1.5d0)*DIMAG(ACurv1%R(i)), &
				& ((Acurv1%k)**1.5d0)*DBLE(ACurv1%RN(i)), ((Acurv1%k)**1.5d0)*DIMAG(ACurv1%RN(i)), &
				& ((Acurv1%k)**1.5d0)*DBLE(ACurv1%Rgr(i)), ((Acurv1%k)**1.5d0)*DIMAG(ACurv1%Rgr(i)), &
				& ((Acurv1%k)**1.5d0)*DBLE(ACurv1%Rde(i)), ((Acurv1%k)**1.5d0)*DIMAG(ACurv1%Rde(i))
!				& ((Acurv1%k)**1.5d0)*DBLE(ACurv1%T(i)), ((Acurv1%k)**1.5d0)*DIMAG(ACurv1%T(i)), &
!				& ((Acurv1%k)**1.5d0)*DBLE(ACurv1%TN(i)), ((Acurv1%k)**1.5d0)*DIMAG(ACurv1%TN(i)), &
!				& ((Acurv1%k)**1.5d0)*DBLE(ACurv1%S(i)), ((Acurv1%k)**1.5d0)*DIMAG(ACurv1%S(i))

!	Ak = -9.0d0*ACurv1%RN(i)/(2.0d0*Field1%Pot(1,i))		!*(1.0d0-eps2*eps2/4.0d0))			! testing - analytical estimate of Sk
!	write(4,*) Field1%efold(i), DBLE(ACurv1%S(i)), DIMAG(ACurv1%S(i)), DBLE(Ak), DIMAG(Ak), DBLE(ACurv1%RN(i)), DIMAG(ACurv1%RN(i))

	end do
	
	Close(2)

	print*
	print*, "Pert_evol.txt written"

deallocate(ACurv1%R)
deallocate(ACurv1%RN)
deallocate(ACurv1%RC)
deallocate(ACurv1%RCN)
deallocate(ACurv1%Rgr)
deallocate(ACurv1%Rde)
deallocate(ACurv1%T)
deallocate(ACurv1%TN)
deallocate(ACurv1%S)

end if
!----------------------------------------------------------------------------------------------------------------------------------------------
if(do_RR) then

	Open(unit = 3, file="PPS_output.txt")
		write(3,*) "#Params of the run :"
		write(3,*) "#phi_i = ", phi_i
		write(3,*) "#e_i = ", e_i
		write(3,*) "#toler = ", toler
		write(3,*) "#m = ", m
		write(3,*) "#phi_0 = ", phi_0
		write(3,*) "#ai = ", ai
		write(3,*) "#kp = ", kp
		write(3,*) "#Np = ", Np
!		write(3,*) "#Nic_con = Nboi = ", Nboi
	if (Nic_func .eq. 1) then
		write(3,*) "#Nic_con = ", Nic_depth, "aH"
	else if (Nic_func .eq. 2) then
		write(3,*) "#Nic_con = ", Nic_depth, "z''/z"
	end if

	write(3,*)
	write(3,*) "# log10(k)		P_S(k)		P_T(k)		n_s-1"

!	dlogk = (log10(kmax)-log10(kmin))/DBLE(NOSD)
	dlogk = (log10(kmax/kmin))/DBLE(NOSD)

	do j=1, NOSD, 1

		y = log10(kmin) + DBLE(j)*(log10(kmax/kmin))/DBLE(NOSD)

		dummy = 0.0d0
		if(j .gt. 2 .and. j .lt. NOSD-2) then
		dummy = (log10(ScalarPower(j))-log10(ScalarPower(j-1)))/(dlogk)				! ns-1 = d(lnPs)/dlnk
		end if


		write(3,*) y, ScalarPower(j), TensorPower(j), dummy, moderror(j), modexit(j)

	end do
	write(3,*)

	print*
	print*, "Power spectra written in PPS_output.txt"

	Close(3)
end if
!----------------------------------------------------------------------------------------------------------------------------------------------
	if(do_RRR) then
		
		Open(unit = 4, file="PBS_output.txt")

		write(4,*) "#Params of the run :"
		write(4,*) "#phi_i = ", phi_i
		write(4,*) "#e_i = ", e_i
		write(4,*) "#toler = ", toler
		write(4,*) "#m = ", m
		write(4,*) "#phi_0 = ", phi_0
		write(4,*) "#ai = ", ai
		write(4,*) "#kp = ", kp
		write(4,*) "#Np = ", Np
!		write(4,*) "#Nic_con = Nboi = ", Nboi
	if (Nic_func .eq. 1) then
		write(4,*) "#Nic_con = ", Nic_depth, "aH"
	else if (Nic_func .eq. 2) then
		write(4,*) "#Nic_con = ", Nic_depth, "z''/z"
	end if

!		write(4,*) "k1		k2		k3		|G_RRR|			f_NL"
!----------------------------------------------------------------------------------------------------------------------------------------------
		if (do_kappa) then

			k1 = k_kappa
			write(4,*) "#k = ", k1
			write(4,*) "#"
			do j3=1, NOSD, 1

			y = -4.0d0 + DBLE(j3)*(5.0d0)/DBLE(NOSD)					! finding kappa
			kappa_trial = 10**y

				dummy = DBLE(Sum_GRRR(j3))
				write(4,*) kappa_trial, (k1**6)*DBLE(componen_RRR(1,j3)),(k1**6)*DBLE(componen_RRR(2,j3)),(k1**6)*DBLE(componen_RRR(3,j3)), &
					& (k1**6)*DBLE(componen_RRR(4,j3)),(k1**6)*DBLE(componen_RRR(5,j3)),(k1**6)*DBLE(componen_RRR(6,j3)), &
					& (k1**6)*DBLE(componen_RRR(7,j3)),(k1**6)*DBLE(componen_RRR(8,j3)),(k1**6)*DBLE(componen_RRR(9,j3)), &
					& (k1**6)*DBLE(componen_RRR(10,j3)),(k1**6)*DBLE(componen_RRR(11,j3)),(k1**6)*DBLE(componen_RRR(12,j3)), &
					& (k1**6)*dummy, fNL(1,j3), fNL(2,j3)
			end do
		print*
		print*, "equilateral bispectrum for k = ",k1,"as a fn. of kappa written in PBS_output.txt"
		end if
!----------------------------------------------------------------------------------------------------------------------------------------------
		if (do_equil) then

			write(4,*) "#kappa = ", kappa
			write(4,*) 
			do j3=1, NOSD, 1

			y = log10(kmin) + DBLE(j3)*(log10(kmax/kmin))/DBLE(NOSD)
			k1 = 10**y

				dummy = DBLE(Sum_GRRR(j3))
				write(4,*) k1, (k1**6)*DBLE(componen_RRR(1,j3)),(k1**6)*DBLE(componen_RRR(2,j3)),(k1**6)*DBLE(componen_RRR(3,j3)), &
					& (k1**6)*DBLE(componen_RRR(4,j3)),(k1**6)*DBLE(componen_RRR(5,j3)),(k1**6)*DBLE(componen_RRR(6,j3)), &
					& (k1**6)*DBLE(componen_RRR(7,j3)),(k1**6)*DBLE(componen_RRR(8,j3)),(k1**6)*DBLE(componen_RRR(9,j3)), &
					& (k1**6)*DBLE(componen_RRR(10,j3)),(k1**6)*DBLE(componen_RRR(11,j3)),(k1**6)*DBLE(componen_RRR(12,j3)), &
					& (k1**6)*dummy, fNL(1,j3), fNL(2,j3)
			end do
		print*
		print*, "Equilateral Bispectrum written in PBS_output.txt"
		end if
!----------------------------------------------------------------------------------------------------------------------------------------------
		if (do_squeez) then

			k3 = ksqueez

			write(4,*) "#kappa = ", kappa
			write(4,*) "#k_squeezed = ", k3
!			write(4,*) "#k_squeezed = k1/1000"
			write(4,*) 

			do j3=1, NOSD, 1
			y = log10(kmin) + DBLE(j3)*(log10(kmax/kmin))/DBLE(NOSD)
			k1 = 10**y
!			k3 = (1.0d-3)*k1

				dummy = DBLE(Sum_GRRR(j3))
				write(4,*) k1, ((k1*k3)**3)*DBLE(componen_RRR(1,j3)),((k1*k3)**3)*DBLE(componen_RRR(2,j3)), &
					& ((k1*k3)**3)*DBLE(componen_RRR(3,j3)), ((k1*k3)**3)*DBLE(componen_RRR(4,j3)), &
					& ((k1*k3)**3)*DBLE(componen_RRR(5,j3)),((k1*k3)**3)*DBLE(componen_RRR(6,j3)), &
					& ((k1*k3)**3)*DBLE(componen_RRR(7,j3)),((k1*k3)**3)*DBLE(componen_RRR(8,j3)), &
					& ((k1*k3)**3)*DBLE(componen_RRR(9,j3)),((k1*k3)**3)*DBLE(componen_RRR(10,j3)), &
					& ((k1*k3)**3)*DBLE(componen_RRR(11,j3)),((k1*k3)**3)*DBLE(componen_RRR(12,j3)), &
					& ((k1*k3)**3)*dummy, fNL(1,j3), fNL(2,j3)
			end do

		print*
		print*, "Squeezed Bispectrum written in PBS_output.txt"
		end if
!----------------------------------------------------------------------------------------------------------------------------------------------
		if(do_2D) then

			k1 = k2D

			do j2=1, NOSD, 1
!			k2 = 0.00d0*k1 + (1.0d0)*k1*DBLE(j2)/DBLE(NOSD)
			k3 = 0.0d0*k1 + (1.0d0)*k1*DBLE(j2)/DBLE(NOSD)			! testing ! CHECK !!!!

				do j3=1, NOSD, 1
!				k3 = 0.0d0*k1 + (1.0d0)*k1*DBLE(j3)/DBLE(NOSD)

				if (k3 .le. k1/2.0d0) then
					k2 = k1-k3 + k3*DBLE(j3)/DBLE(NOSD)		! testing
				else
					k2 = k3 + (k1-k3)*DBLE(j3)/DBLE(NOSD)		! testing
				end if
			
					jj1 = (j2-1)*NOSD+j3
	
					dummy = DBLE(Sum_GRRR_2D(jj1))

					if(fNL_2D(jj1) /= 0.0d0) then

					write(4,*) k1, k2, k3, dummy, fNL_2D(jj1)
!					write(4,*) k2/k1, k3/k1, fNL_2D(jj1)

					end if
				end do
				write(4,*)
			end do
		print*
		print*, "Bispectrum written in PBS_output.txt"
		end if
!----------------------------------------------------------------------------------------------------------------------------------------------
		Close(4)
!----------------------------------------------------------------------------------------------------------------------------------------------
		if(do_3D) then

		Open(unit = 5, file="PBS_3D_output.txt")
		write(5,*) "k1		k2		k3		|G_RRR|			f_NL"
	
		do j1=1, NOSD, 1
		k1 = 10**(-6.0d0 + (6.0d0)*DBLE(j1)/DBLE(NOSD))
	
			do j2=1, NOSD, 1
			k2 = 0.0d0 + k1*DBLE(j2)/DBLE(NOSD)
	
				do j3=1, NOSD, 1
				k3 = 0.0d0 + k1*DBLE(j3)/DBLE(NOSD)
				
				if(fNL(1,(j1-1)*NOSD*NOSD+(j2-1)*NOSD+j3) /= 0.0d0) then

				dummy = dble(Sum_GRRR((j1-1)*NOSD*NOSD+(j2-1)*NOSD+j3))
				write(5,*) k1, k2, k3, dummy, fNL(1,(j1-1)*NOSD*NOSD+(j2-1)*NOSD+j3)
	
				end if
	
				end do
				write(5,*)
			end do
			write(5,*)

		end do
		write(5,*)

			print*
			print*, "Bispectrum and f_NL written in PBS_3D_output.txt"

		Close(5)
		end if

!----------------------------------------------------------------------------------------------------------------------------------------------
	end if
!----------------------------------------------------------------------------------------------------------------------------------------------
end if
!----------------------------------------------------------------------------------------------------------------------------------------------


end if

call cpu_time(time2)

deallocate(Field1%phi)
deallocate(Field1%phiN)				
deallocate(Field1%Pot)				
deallocate(Field1%H)
deallocate(Field1%z)
deallocate(Field1%phiNN)
deallocate(Field1%phiNNN)
deallocate(Field1%a)

!----------------------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------------------------------------

CONTAINS

!----------------------------------------------------------------------------------------------------------------------------------------------
Subroutine back_solver(NOOS, Field1, ACurv, maxerr)

Integer :: i, j, NOOS
Double precision :: e, s, dummy, berr, maxerr
Complex*16 :: p, pd
Type(BG) :: Field1
Type(Pert) :: ACurv

maxerr = 0.0d0

s = 1.0d0/DBLE(NOS)
Field1%estep(1) = 1.0d0/DBLE(NOS)

!do while(i .lt. NOOS)
do i=1, NOOS, 1

	e = Field1%efold(i)
	s = Field1%estep(i)
	p = DCMPLX(Field1%phi(i))
	pd = DCMPLX(Field1%phiN(i))

	Call RKadap(i,e,s,p,pd,f1,f2,Field1,ACurv,berr)

!	Call RKF5(i,e,s,p,pd,f1,f2,Field1,ACurv,berr)
!	Call RKCK5(i,e,s,p,pd,f1,f2,Field1,ACurv,berr)				! Direct calls for Linear solving
!	Call RKDP5(i,e,s,p,pd,f1,f2,Field1,ACurv,berr)

	if (e .lt. DBLE(Nf)) then
		if (j .ne. NOOS) then
			if (berr .lt. toler) then
			e = e+s
			j = i+1							! i+1 because y & yd corr. to t+h time step - for do loop
			Call backqties(j,e,s,p,pd,Field1)

				dummy = Field1%phiN(i)*Field1%phiN(i)/2.0d0		! checking for end of inflation when eps1 = 1.5
				if ((e .ge. 60.0d0) .and. (dummy .ge. 1.5d0)) then
!				if (dummy .ge. 1.5d0) then
				print* 
				print*, "Nend = ", e
				exit
				end if

!				dummy = abs(Field1%phi(i))
!				if (dummy .le. 1.0d-4) then					! checking for end of inflation when phi ~ 0
!				dummy = Field1%phi(i)
!				if (dummy .le. -0.4d0) then					! checking end of inflation at non-zero phi
!				print* 
!				print*, "Nend = ", e
!				exit
!				end if

				if(berr .gt. maxerr) then
				maxerr=berr
				end if

			else
				write(*,*)
				write(*,*) "TOL EXC at N=",e,"delN=",s,"error=",berr
				exit
			end if			

		else if (i .eq. NOOS) then
			
			print*, "i overshoot for back evol."
			STOP
		else

			print*, "check i=",i,"for back evol."
		end if


	else if (e .ge. Nf) then

		print*, "Nf reached for background", e
		exit
	else
		print*, "check N =",N,"for background"
		exit
	end if


end do

End subroutine back_solver

!----------------------------------------------------------------------------------------------------------------------------------------------

Subroutine Pert_solver(Field1,ACurv,ks,kl)

Integer :: i, ip, istart, iend, ievol
Double Precision :: e, s, ks, kl, Nstart, Nend, zeebyz, dummy
Complex*16 :: p, pd, q, qd, p1, p1d, q1, q1d

Type(BG) :: Field1
Type(Pert) :: ACurv

ACurv%i_a = 1
ACurv%i_s = 1

ACurv%Nic=0
ACurv%Nstar=0
ACurv%Nshs=0

ACurv%R = 0
ACurv%RN = 0

ACurv%RC = 0
ACurv%RCN = 0

ACurv%T = 0
ACurv%TN = 0

ACurv%SPS = 0.0d0
ACurv%TPS = 0.0d0

maxerr = 0.0d0

do i=1, Field1%NOOOS, 1								! index 'i' loop; searching through the sampled points of evolution of phi and backqties

	e = Field1%efold(i)

	if (Nic_func .eq. 1) then
		dummy = (Nic_depth)*(ai*dexp(e)*Field1%H(1,i))
	else if (Nic_func .eq. 2) then
		zeebyz = abs(Field1%z(3,i))
		dummy = Nic_depth*(sqrt(zeebyz))
	else
		print*
		print*, "specify a valid Nic function ! Stopping run !"
		STOP
	end if

	ACurv%ic_con = (ks-dummy)

	if (ACurv%ic_con .le. 0.0d0) then					! precision expected vs. possible ???? note: dk ~ k as dlog(k)~const.
	ACurv%i_a = i											! index corr. to ini. condn.
	ACurv%Nic = e											! e-fold of initial condn.
	exit
	end if
end do

do i=ACurv%i_a, Field1%NOOOS, 1

	e = Field1%efold(i)
!	zeebyz = abs(Field1%z(3,i))
!	dummy = (1.0d0)*(sqrt(zeebyz))

	ACurv%star_con = (ACurv%k-1.0d0*(ai*dexp(e)*Field1%H(1,i)))
!	ACurv%star_con = (ACurv%k-dummy)

	if(ACurv%star_con .le. 0.0d0) then					! precision expected vs. possible ????

	ACurv%i_s = i											! index corr. to hubble exit
	ACurv%Nstar = e											! e-fold of hubble exit
	exit
	end if
end do

do i=ACurv%i_s, Field1%NOOOS, 1

	e = Field1%efold(i)
!	zeebyz = abs(Field1%z(3,i))
!	dummy = (1.0d-5)*(sqrt(zeebyz))

!!	ACurv%shs_con = (ACurv%k-1.0d-5*(ai*dexp(e)*Field1%H(1,i)))
	ACurv%shs_con = (kl-(1.0d-5)*(ai*dexp(e)*Field1%H(1,i)))
!	ACurv%shs_con = (kl-dummy)

	if(ACurv%shs_con .le. 0.0d0) then					! precision expected vs. possible ????

	ACurv%i_z = i											! index corr. to hubble exit
	ACurv%Nshs = e											! e-fold of hubble exit
	exit
	end if
end do

!----------------------------------------------------------------------------------------------------------------------------------------------
if (ACurv%Nic .eq. 0.0d0) then
ACurv%i_a = iboi
ACurv%Nic = Nboi
end if

if (ACurv%Nshs .eq. 0.0d0) then
ACurv%i_z = ieoi
ACurv%Nshs = Neoi						! all modes evolved till the end of inflation
end if
!----------------------------------------------------------------------------------------------------------------------------------------------

istart = ACurv%i_a
iend = ACurv%i_z
ievol = iend !- istart + 1

Nstart = ACurv%Nic
Nend = ACurv%Nshs

	ACurv%R(istart) = DCMPLX(1/(Field1%z(1,istart)*sqrt(2*ACurv%k)),0) 				! initial conditions for scalar modes
	ACurv%RN(istart) = DCMPLX(-Field1%z(2,istart)/(sqrt(2*ACurv%k)*Field1%z(1,istart)**2), &
			& -(sqrt(ACurv%k/2)/(Field1%z(1,istart)*ai*exp(Nstart)*Field1%H(1,istart))))

	ACurv%T(istart) = DCMPLX(1/(ai*dexp(Nstart)*sqrt(2*ACurv%k)),0)					! initial conditions for tensor modes
	ACurv%TN(istart) = DCMPLX(-1/(ai*dexp(Nstart)*sqrt(2*ACurv%k)), &
			& -(sqrt(ACurv%k/2)/(ai*ai*dexp(2*Nstart)*Field1%H(1,istart))))

!do ip=1, ievol, 1								! index loop
do i=istart, iend, 1

	ip = i

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		p = ACurv%R(ip)
		pd = ACurv%RN(ip)

!		Call RKF5(i,e,s,p,pd,f3,f4,Field1,ACurv,ACurv%Rerr)			! the assumption here is that features in R-evolution coincides with those in phi-evolution; 
											!else we need interpolation of backgnd. qties (??????)
		Call RKCK5(i,e,s,p,pd,q,qd,f3,f4,Field1,ACurv,ACurv%Rerr)

		ACurv%R(ip+1) = q
		ACurv%RN(ip+1) = qd

		p = ACurv%T(ip)
		pd = ACurv%TN(ip)

!		Call RKF5(i,e,s,p,pd,f5,f6,Field1,ACurv,ACurv%Terr)			! the assumption here is that features in T-evolution coincides 
											! with those in phi-evolution;										
											! else we need interpolation of backgnd. qties (??????)
		Call RKCK5(i,e,s,p,pd,q,qd,f5,f6,Field1,ACurv,ACurv%Terr)

		ACurv%T(ip+1) = q
		ACurv%TN(ip+1) = qd

		maxerr = maxerr + ACurv%Rerr

end do										! e-fold loop

	ACurv%SPS = (ACurv%k**3)*(DBLE(ACurv%R(ievol))**2+DIMAG(ACurv%R(ievol))**2)/(2*PI*PI)
!	ACurv%SSRA = Field1%Pot(1,ACurv%i_s)**3/(12.*(PI*Field1%Pot(2,ACurv%i_s))**2)			! Slow roll approx. of Scalar power spectrum

	ACurv%TPS = 8.0d0*(ACurv%k**3)*(DBLE(ACurv%T(ievol))**2+DIMAG(ACurv%T(ievol))**2)/(2*PI**2)	! factor of 8 in front if T = u/a - ref. Daniel Baumann, Primordial Cosmology, TASI lectures, Sec.:7.3
!	ACurv%TPS = 4.0d0*(ACurv%k**3)*(DBLE(ACurv%T(ievol))**2+DIMAG(ACurv%T(ievol))**2)/(2*PI*PI)
!	ACurv%TSRA = 4.0d0*Field1%Pot(1,ACurv%i_s)/(3.*PI**2)

ACurv%Rerr = maxerr

End subroutine Pert_solver
!----------------------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------------------------------------

Subroutine RRR_integrator(Field1,ACurv1,ACurv2,ACurv3,kappa,GRRR,S_GRRR,ngp)

Integer :: i, j, ip1, ip2, ip3, istart, iend
Integer :: istart1, istar1, iend1, ievol1
Integer :: istart2, istar2, iend2, ievol2
Integer :: istart3, istar3, iend3, ievol3
Double Precision :: dn, Nstart, Nend, eps1_start, eps1_end, eps2_start, eps2_end
Double Precision :: costh12, costh23, costh13
Double Precision :: k1, k2, k3, k1k1, k2k2, k3k3, k1k2, k2k3, k1k3, SPS1, SPS2, SPS3
Double Precision :: k1k1k1, k2k2k2, k3k3k3, k1k2k3
Double Precision :: cutoff, t7, kappa, dummy2
Double Precision, dimension(2) :: ngp
Complex*16 :: S_GRRR, dummy
Complex*16, dimension(12) :: GRRR
Complex*16 :: t1,t2,t3,t4,t5,t6

Double precision :: ef								! testing

Type(BG) :: Field1
Type(Pert) :: ACurv1, ACurv2, ACurv3

do i=1, Field1%NOOOS, 1

ACurv1%RC(i) = DCONJG(ACurv1%R(i))
ACurv1%RCN(i) = DCONJG(ACurv1%RN(i))
ACurv2%RC(i) = DCONJG(ACurv2%R(i))
ACurv2%RCN(i) = DCONJG(ACurv2%RN(i))
ACurv3%RC(i) = DCONJG(ACurv3%R(i))
ACurv3%RCN(i) = DCONJG(ACurv3%RN(i))

end do

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

k1k1k1 = k1*k1*k1
k2k2k2 = k2*k2*k2
k3k3k3 = k3*k3*k3
k1k2k3 = k1*k2*k3

SPS1 = ACurv1%SPS
SPS2 = ACurv2%SPS
SPS3 = ACurv3%SPS

istart = ACurv1%i_a
Nstart = ACurv1%Nic

Nend = ACurv1%Nshs								! Note : Nend = ACurv1%Nshs = ACurv2%Nshs = ACurv3%Nshs 
iend = ACurv1%i_z								! since all modes end evolving at ACurv%Nshs = Field1%efold(Field1%NOOOS-200)

istart1 = ACurv1%i_a
istar1 = ACurv1%i_s
iend1 = ACurv1%i_z
ievol1 = iend1 !- istart1 + 1

istart2 = ACurv2%i_a
iend2 = ACurv2%i_z
ievol2 = iend2 !- istart2 + 1

istart3 = ACurv3%i_a
iend3 = ACurv3%i_z								! all modes have same 'istart' and 'iend' based on 'ks' and 'kl'
ievol3 = iend3 !- istart3 + 1 

GRRR = 0.0d0
S_GRRR=0.0d0
ngp = 0.0d0

!----------------------------------------------------------------------------------------------------------------------------------------------

!do i=1, Field1%NOOOS, 1	

!	eps1 = Field1%phiN(i)*Field1%phiN(i)/2
!	t7 = Field1%efold(i)

!if (eps1 .le. 1.0d-2) then
!if (t7 .ge. 5.0d0) then

!istart1 = i							! testing
!istart2 = i	
!istart3 = i	

!print*, "Nstart = ", Field1%efold(i)

!exit
!end if

!end do

!if (thrid .eq. 0) then
!print*, "Nend of integral = ", Nend
!end if

!Nend = 2.0d0
!do i=1, Field1%NOOOS, 1	

!	ef = Field1%efold(i)

!if (ef .ge. Nend) then

!iend1 = i							! testing
!iend2 = i	
!iend3 = i	

!ievol1 = i	
!ievol2 = i	
!ievol3 = i	

!print*, "Nend = ", Field1%efold(i)

!exit
!end if

!end do

!----------------------------------------------------------------------------------------------------------------------------------------------
if (do_boundaries) then
!----------------------------------------------------------------------------------------------------------------------------------------------
!Surface term - G7(Nic)

	GRRR(8) = GRRR7(istart,Field1,ACurv1,ACurv2,ACurv3,kappa)

S_GRRR = S_GRRR - GRRR(8)

!----------------------------------------------------------------------------------------------------------------------------------------------
! Surface terms - G_Bound_1(Nic)

	GRRR(10) = GRRRB1(istart,Field1,ACurv1,ACurv2,ACurv3,kappa)

S_GRRR = S_GRRR - GRRR(10)

!----------------------------------------------------------------------------------------------------------------------------------------------
! Surface terms - G_Bound_2(Nic)

	GRRR(12) = GRRRB2(istart,Field1,ACurv1,ACurv2,ACurv3,kappa)

S_GRRR = S_GRRR - GRRR(12)

!----------------------------------------------------------------------------------------------------------------------------------------------
end if
!----------------------------------------------------------------------------------------------------------------------------------------------

if (do_bulk) then
do i=istart1, iend1, 4

t1 = 0
t2 = 0
t3 = 0
t4 = 0
t5 = 0
t6 = 0
dummy = 0.0d0
		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		Call Integ(i,e,s,GRRR1,t1,Field1,ACurv1,ACurv2,ACurv3,kappa)
		Call Integ(i,e,s,GRRR2,t2,Field1,ACurv1,ACurv2,ACurv3,kappa)
		Call Integ(i,e,s,GRRR3,t3,Field1,ACurv1,ACurv2,ACurv3,kappa)
		Call Integ(i,e,s,GRRR4,t4,Field1,ACurv1,ACurv2,ACurv3,kappa)
		Call Integ(i,e,s,GRRR5,t5,Field1,ACurv1,ACurv2,ACurv3,kappa)
		Call Integ(i,e,s,GRRR6,t6,Field1,ACurv1,ACurv2,ACurv3,kappa)

GRRR(1) = GRRR(1) + t1
GRRR(2) = GRRR(2) + t2
GRRR(3) = GRRR(3) + t3
GRRR(4) = GRRR(4) + t4
GRRR(5) = GRRR(5) + t5
GRRR(6) = GRRR(6) + t6

S_GRRR = S_GRRR + t1+t2+t3+t4+t5+t6
	
end do
end if

!----------------------------------------------------------------------------------------------------------------------------------------------
!Surface term - G7(Nend)

	GRRR(7) = GRRR7(iend,Field1,ACurv1,ACurv2,ACurv3,kappa)

S_GRRR = S_GRRR + GRRR(7)

!----------------------------------------------------------------------------------------------------------------------------------------------
if (do_boundaries) then
!----------------------------------------------------------------------------------------------------------------------------------------------
! Surface terms - G_Bound_1(Nend)

	GRRR(9) = GRRRB1(iend,Field1,ACurv1,ACurv2,ACurv3,kappa)

S_GRRR = S_GRRR + GRRR(9)

!----------------------------------------------------------------------------------------------------------------------------------------------
! Surface terms - G_Bound_2(Nend)

	GRRR(11) = GRRRB2(iend,Field1,ACurv1,ACurv2,ACurv3,kappa)

S_GRRR = S_GRRR + GRRR(11)

!----------------------------------------------------------------------------------------------------------------------------------------------
end if
!----------------------------------------------------------------------------------------------------------------------------------------------

!! Surface term - G7(Nend) - usual

!eps2_end = 2.0d0*Field1%phiNN(iend)/Field1%phiN(iend)				! eps_2 = 2*phiNN/phiN

!t1 = abs(ACurv1%RC(ievol1))*abs(ACurv2%RC(ievol2))
!t2 = abs(ACurv2%RC(ievol2))*abs(ACurv3%RC(ievol3))
!t3 = abs(ACurv1%RC(ievol1))*abs(ACurv3%RC(ievol3))

!GRRR(7) = eps2_end*(t1*t1 + t2*t2 + t3*t3)/(2.0d0)

!S_GRRR = S_GRRR + GRRR(7)

!----------------------------------------------------------------------------------------------------------------------------------------------
! Aliter:
!----------------------------------------------------------------------------------------------------------------------------------------------
!do i=istart1, iend1, 4

!t1 = 0
!t2 = 0
!t3 = 0
!t4 = 0
!t5 = 0
!t6 = 0
!dummy = 0.0d0
!		e = Field1%efold(i)
!		s = Field1%estep(i+1)						! step size corr. to index ?check?

!		Call Integ(i,e,s,GRRR123,t3,Field1,ACurv1,ACurv2,ACurv3,0.0d0)
!		Call Integ(i,e,s,GRRR4,t4,Field1,ACurv1,ACurv2,ACurv3,kappa)
!		Call Integ(i,e,s,GRRR5,t5,Field1,ACurv1,ACurv2,ACurv3,kappa)
!		Call Integ(i,e,s,GRRR6,t6,Field1,ACurv1,ACurv2,ACurv3,kappa)

!GRRR(1) = GRRR(1) + t1
!GRRR(2) = GRRR(2) + t2
!GRRR(3) = GRRR(3) + t3
!GRRR(4) = GRRR(4) + t4
!GRRR(5) = GRRR(5) + t5
!GRRR(6) = GRRR(6) + t6

!S_GRRR = S_GRRR + t1+t2+t3+t4+t5+t6
	
!end do
!----------------------------------------------------------------------------------------------------------------------------------------------
!Surface term - G8

!eps1_end = Field1%phiN(iend)*Field1%phiN(iend)/2.0d0				! eps_1 = phiN/phiN/2

!t1 = ACurv1%R(ievol1)*ACurv1%RC(ievol1)*ACurv2%R(ievol2)*ACurv2%RC(ievol2)*ACurv3%R(ievol3)*ACurv3%RCN(ievol3)
!t2 = ACurv1%R(ievol1)*ACurv1%RC(ievol1)*ACurv2%R(ievol2)*ACurv2%RCN(ievol2)*ACurv3%R(ievol3)*ACurv3%RC(ievol3)
!t3 = ACurv1%R(ievol1)*ACurv1%RCN(ievol1)*ACurv2%R(ievol2)*ACurv2%RC(ievol2)*ACurv3%R(ievol3)*ACurv3%RC(ievol3)

!t1 = t1*k1k2/k3k3
!t2 = t2*k1k3/k2k2
!t3 = t3*k2k3/k1k1

!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*ai*dexp(Nend)*Field1%H(1,iend)))

!GRRR(1) = 2*II*ai*ai*ai*dexp(3*Nend)*eps1_end*eps1_end*(t1 + t2 + t3)*Field1%H(1,iend)*cutoff			! G8(end)

!---------------------------------------------------------

!eps1_start = Field1%phiN(istart1)*Field1%phiN(istart1)/2.0d0					! eps_1 = phiN/phiN/2

!t4 = ACurv1%R(ievol1)*ACurv1%RC(istart1)*ACurv2%R(ievol2)*ACurv2%RC(istart2)*ACurv3%R(ievol3)*ACurv3%RCN(istart3)
!t5 = ACurv1%R(ievol1)*ACurv1%RC(istart1)*ACurv2%R(ievol2)*ACurv2%RCN(istart2)*ACurv3%R(ievol3)*ACurv3%RC(istart3)
!t6 = ACurv1%R(ievol1)*ACurv1%RCN(istart1)*ACurv2%R(ievol2)*ACurv2%RC(istart2)*ACurv3%R(ievol3)*ACurv3%RC(istart3)

!t4 = t4*k1k2/k3k3
!t5 = t5*k1k3/k2k2
!t6 = t6*k2k3/k1k1

!GRRR(1) = GRRR(1) - 2*II*ai*ai*ai*dexp(3*Nstart)*eps1_start*eps1_start*(t4 + t5 + t6)*Field1%H(1,istart1)*cutoff	! G8(initial)

!GRRR(1) = GRRR(1) + DCONJG(GRRR(1))								! G8 + c.c. testing - sign

!S_GRRR = S_GRRR + GRRR(1)
!----------------------------------------------------------------------------------------------------------------------------------------------
!if G123 instead of G1+G2+G3
!Surface term - G9

!eps1_end = Field1%phiN(iend)*Field1%phiN(iend)/2.0d0				! eps_1 = phiN*phiN/2

!t1 = ai*dexp(Nend)*Field1%H(1,iend)

!t2 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
!t3 = (1.0/k1k1 + 1.0/k2k2 +1.0/k3k3)*ACurv1%RCN(ievol1)*ACurv2%RCN(ievol2)*ACurv3%RCN(ievol3)*t1*t1*t1

!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*ai*dexp(Nend)*Field1%H(1,iend)))

!GRRR(2) = -2*II*ai*ai*dexp(2*Nend)*eps1_end*eps1_end*t2*t3*cutoff 					! G9(end)

!---------------------------------------------------------

!eps1_start = Field1%phiN(istart1)*Field1%phiN(istart1)/2.0d0				! eps_1 = phiN*phiN/2

!t4 = ai*dexp(Nstart)*Field1%H(1,istart1)

!t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
!t6 = (1.0/k1k1 + 1.0/k2k2 + 1.0/k3k3)*ACurv1%RCN(istart1)*ACurv2%RCN(istart2)*ACurv3%RCN(istart3)*t4*t4*t4

!GRRR(2) = GRRR(2) - (-2*II*ai*ai*dexp(2*Nstart)*eps1_start*eps1_start*t5*t6)*cutoff			! G9(initial)

!GRRR(2) = GRRR(2) + DCONJG(GRRR(2))								! G9 + c.c.

!S_GRRR = S_GRRR + GRRR(2)
!----------------------------------------------------------------------------------------------------------------------------------------------
! Aliter ends
!----------------------------------------------------------------------------------------------------------------------------------------------

t1 = k1k1k1*SPS2*SPS3
t2 = k2k2k2*SPS1*SPS3
t3 = k3k3k3*SPS1*SPS2

t7 = DBLE(t1+t2+t3)

dummy = DBLE(S_GRRR)

ngp(1) = -(10.0d0/(3.0d0*(2.0d0*PI)**4))*(k1k2k3**3)*dummy/t7

dummy = Field1%phiN(ACurv1%i_s)*Field1%phiN(ACurv1%i_s)/2.0d0
dummy2 = 2.0d0*Field1%phiNN(ACurv1%i_s)/Field1%phiN(ACurv1%i_s)

if(do_squeez) then
ngp(2) = -8.5d1*dummy/3.6d1							! SR analytical estimate of fNL_sq
else if (do_equil .or. do_kappa) then
ngp(2) = -5.0d0*(11.0d0*dummy + 3.0d0*dummy2)/3.6d1				! SR analytical estimate of fNL_eq
end if

End subroutine RRR_integrator

!----------------------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------------------------------------
END PROGRAM PBS
!----------------------------------------------------------------------------------------------------------------------------------------------
