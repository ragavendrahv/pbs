MODULE RK_45					! modified specifically for standalone PPS

Use params
Use hv

Implicit none

Contains 
													! rk45 modified from rk45vec from SSS.
Recursive Subroutine rk45_step(j, t0, y0, yd0, h, f1, f2, y, yd, er, field)

	Integer :: j, i											! j= time index to track & storebackqties
	Double precision :: t0, h
	COMPLEX*16 :: y0, yd0										! h = step size in t
	COMPLEX*16 :: y, y1, y2, y3, y4, y5, yd, yd1, yd2, yd3, yd4, yd5
	COMPLEX*16 :: k0, k1, k2, k3, k4, k5, l0, l1, l2, l3, l4, l5
	COMPLEX*16 :: er, e1, e2
	Type(BG) :: field
	DOUBLE PRECISION, dimension(6):: c = (/0.00, 0.25, 0.375, 0.923077, 1.00, 0.50/), t
	DOUBLE PRECISION, dimension(6,6) :: a = transpose(reshape((/ +0.000000, +0.000000, +0.000000, +0.000000, +0.000000, +0.000000, &
									+0.250000, +0.000000, +0.000000, +0.000000, +0.000000, +0.000000, &
						 			+0.093750, +0.281250, +0.000000, +0.000000, +0.000000, +0.000000, &
									+0.879381, -3.277196, +3.320892, +0.000000, +0.000000, +0.000000, &			! check ordering
									+2.032407, -8.000000, +7.173489, -0.205897, +0.000000, +0.000000, &
									-0.296296, +2.000000, -1.381676, +0.452973, -0.275000, +0.000000  /), shape(a)))

	DOUBLE PRECISION, dimension(6) :: b5 = (/+0.118519, +0.000000, +0.518986, +0.506131, -0.180000, +0.036364/)
!	/*double b4(6) = {+0.115741, +0.000000, +0.548928, +0.535331, -0.200000, +0.000000};*/
	DOUBLE PRECISION, dimension(6) :: b45= (/+0.002778, +0.000000, -0.029942, -0.029200, +0.020000, +0.036364/)

interface

	Complex*16 Function f1(j,x,yi,ydi,field)			! interface for functions passed
	use params
	integer :: j
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG)::field
	end function f1

	Complex*16 Function f2(j,x,yi,ydi,field)
	use params
	integer :: j
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG)::field
	end function f2

end interface

	do i = 1, 6, 1
		t(i) = t0 + h * c(i)
	end do

	k0 = f1(j, t0, y0, yd0, field)
	l0 = f2(j, t0, y0, yd0, field)

		y1 = y0 + h * a(2,1) * k0
		yd1 = yd0 + h * a(2,1) * l0
	k1 = f1(j, t(1), y1, yd1, field)
	l1 = f2(j, t(1), y1, yd1, field)

		y2 = y0 + h * (a(3,1) * k0 + a(3,2) * k1)
		yd2 = yd0 + h * (a(3,1) * l0 + a(3,2) * l1)
	k2 = f1(j, t(2), y2, yd2, field)
	l2 = f2(j, t(2), y2, yd2, field)

		y3 = y0 + h * (a(4,1) * k0 + a(4,2) * k1 + a(4,3) * k2)
		yd3 = yd0 + h * (a(4,1) * l0 + a(4,2) * l1 + a(4,3) * l2)
	k3 = f1(j, t(3), y3, yd3, field)
	l3 = f2(j, t(3), y3, yd3, field)


		y4 = y0 + h * (a(5,1) * k0 + a(5,2) * k1 + a(5,3) * k2 + a(5,4) * k3)
		yd4 = yd0 + h * (a(5,1) * l0 + a(5,2) * l1 + a(5,3) * l2 + a(5,4) * l3)
	k4 = f1(j, t(4), y4, yd4, field)
	l4 = f2(j, t(4), y4, yd4, field)

		y5 = y0 + h * (a(6,1) * k0 + a(6,2) * k1 + a(6,3) * k2 + a(6,4) * k3 + a(6,5) * k4)
		yd5 = yd0 + h * (a(6,1) * l0 + a(6,2) * l1 + a(6,3) * l2 + a(6,4) * l3 + a(6,5) * l4)  
	k5 = f1(j, t(5), y5, yd5, field)
	l5 = f2(j, t(5), y5, yd5, field)


		y = y0 + h*(b5(1)*k0 + b5(2)*k1 + b5(3)*k2 + b5(4)*k3 + b5(5)*k4 + b5(6)*k5)
		yd = yd0 + h*(b5(1) * l0 + b5(2) * l1 + b5(3) * l2 + b5(4) * l3 + b5(5) * l4 + b5(6) * l5)

		e1 = 0
		e2 = 0
		e1 = (h * (b45(1) * k0 + b45(2) * k1 + b45(3) * k2 + b45(4) * k3 + b45(5) * k4 + b45(6) * k5) &
			& - h * (b5(1) * k0 + b5(2) * k1 + b5(3) * k2 + b5(4) * k3 + b5(5) * k4 + b5(6) * k5))	 	! error in y 
		e1 = abs(e1)/abs(y)
!		e2 = abs(h * (b45(1) * l0 + b45(2) * l1 + b45(3) * l2 + b45(4) * l3 + b45(5) * l4 + b45(6) * l5) &
!			  & - h * (b5(1) * l0 + b5(2) * l1 + b5(3) * l2 + b5(4) * l3 + b5(5) * l4 + b5(6) * l5)) 	! + error in yd = error in both
!		e2 = abs(e2)/abs(yd)
		er = e1+e2
		
		if( (abs(er) .ge. toler) .and. (h .ge. 1.0d-16) ) then										

			h = h/2.0d0											! repeat the step with half the stepsize
!			print*, t0, abs(e1), abs(y), abs(e2), abs(yd), h			
!			assert(eg(i) .lt. toler)
			Call rk45_step(j, t0, y0, yd0, h, f1, f2, y, yd, er, field)
		else
			continue
		end if

End subroutine rk45_step
!----------------------------------------------------------------------------------------------------------------------------------------------
Subroutine rk45(t0, tn, h, n, y0, yd0, f1, f2, qties, field)

	Double Precision :: t0, tn, h
	Complex*16 :: y0, yd0
	Complex*16 :: y, yd, eg
	Double Precision :: t, maxerr
	Integer :: n, i, j
	Type(BG)::field

interface

	Subroutine qties(i,e,h,p,pd,field)		! interface for subroutine called (necessary ?)
	use params
	Integer :: i 
	Double precision :: e, h
	Complex*16 :: p,pd
	Type(BG)::field
	end subroutine qties

	Complex*16 Function f1(i,x,yi,ydi,field)			! interface for functions passed
	use params
	integer :: i
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG)::field
	end function f1

	Complex*16 Function f2(i,x,yi,ydi,field)
	use params
	integer :: i
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG)::field
	end function f2

end interface

!	assert(eg)

	eg=0
	maxerr = 0
!	n = INT((tn-t0)/h)						! # of time steps => h = (tn-t0)/n std. stepsize
	t = t0

	do i=1, (n+1), 1						! index loop, arb large limit ?? / no. of sampling pts affordable
	if(t .le. tn) then

		Call rk45_step(i, t, y0, yd0, h, f1, f2, y, yd, eg, field)
		
!!		Call RK5(j,xi,h,yi,ydi,func1,func2,field,pertur,er)

		if(DBLE(eg) .lt. toler) then					! second check to ensure error < tolerance
!			write(*,*) DBLE(eg),t,h
			y0 = y   
			yd0=yd

			t = t+h	
			j = i+1							! i+1 because y & yd corr. to t+h time step
			Call qties(j,t,h,y,yd,field)
!			h = (100*(tn-t0))/DBLE(n)					! restore std. stepsize, size above which we miss out features
!			h = ((tn-t0)/DBLE(NOS))
			if (h .le. 1.0d1/DBLE(NOS*Nadap)) then		! preventing stepsize from drop below optimal (?subjective?) step size 
			h=2.0d0*h	
			else
				continue							
			end if

		else
			write(*,*)
			write(*,*) "TOL EXC at t=",t,"h=",h		
		exit
		end if			

		if(DBLE(eg) .gt. maxerr) then
			maxerr=eg
		end if
	
	else 
		exit
	end if
	end do

	write(*,*)"MAX ERROR:",maxerr


End Subroutine rk45
!----------------------------------------------------------------------------------------------------------------------------------------------
Subroutine RK4(i,xi,h,yi,ydi,func1,func2,field,pertur,er)

Integer :: i
Double Precision :: xi, h, x, er
Complex*16 :: yi, ydi 
Complex*16 :: func1, func2
Complex*16 :: y, z, zi
Complex*16, dimension (4) :: k, l

Type(BG) :: field
Type(Pert)::pertur

interface 
	Function func1(i,x,yi,ydi,field,pertur)
	use params
	integer :: i
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG) :: field
	Type(Pert) :: pertur
	end function func1

	Function func2(i,x,yi,ydi,field,pertur)
	use params
	integer :: i	
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG) :: field
	Type(Pert) :: pertur	

	end function func2
end interface

!y = yi + 5*k(1)/48 + 27*k(2)/56 + 125*k(3)/336 + k(4)/24 ??? RK-5

zi = ydi

k(1) = func1(i,xi,yi,zi,field,pertur)
l(1) = func2(i,xi,yi,zi,field,pertur)

x = xi+h/2.
y = yi+h*k(1)/2.
z = zi+h*l(1)/2.
k(2) = func1(i, x, y, z, field, pertur)
l(2) = func2(i, x, y, z, field, pertur)

x = xi+h/2.
y = yi+h*k(2)/2.
z = zi+h*l(2)/2.
k(3) = func1(i, x, y, z, field, pertur)
l(3) = func2(i, x, y, z, field, pertur)

x = xi+h
y = yi+h*k(3)
z = zi+h*l(3)
k(4) = func1(i, x, y, z, field, pertur)
l(4) = func2(i, x, y, z, field, pertur)

y = yi + h*(k(1) + 2.*(k(2) + k(3)) + k(4))/6.
z = zi + h*(l(1) + 2.*(l(2) + l(3)) + l(4))/6.

yi = y
ydi = z

er = abs((h/6)*((l(1)+2*(l(2)+l(3))+l(4)) + (k(1) + 2.*(k(2) + k(3)) + k(4))))			! check for stiffness

!if (abs(er) .ge. 1.0d-7) then
!print*, "Error overshoot in Rk evol:", er
!end if

end Subroutine RK4
!----------------------------------------------------------------------------------------------------------------------------------------------
Subroutine RKF5(j,xi,h,yi,ydi,func1,func2,field,pertur,er)

Integer :: i, j
Double Precision :: xi, h, e1, e2, er
Complex*16 :: yi, ydi 
Complex*16 :: func1, func2
Complex*16 :: y, yd
Complex*16, dimension (6) :: k, l
COMPLEX*16 :: e

DOUBLE PRECISION, dimension(5):: c = (/0.25, 0.375, 0.923077, 1.00, 0.50/), x
DOUBLE PRECISION, dimension(25) :: a = (/+0.250000, +0.000000, +0.000000, +0.000000, +0.000000, &
					 +0.093750, +0.281250, +0.000000, +0.000000, +0.000000, &
					 +0.879381, -3.277196, +3.320892, +0.000000, +0.000000, &			! check ordering
					 +2.032407, -8.000000, +7.173489, -0.205897, +0.000000, &
					 -0.296296, +2.000000, -1.381676, +0.452973, -0.275000/)

DOUBLE PRECISION, dimension(6) :: b5 = (/+0.118519, +0.000000, +0.518986, +0.506131, -0.180000, +0.036364/)
!DOUBLE PRECISION, dimension (6) :: b4 = {+0.115741, +0.000000, +0.548928, +0.535331, -0.200000, +0.000000};
DOUBLE PRECISION, dimension(6) :: b45= (/+0.002778, +0.000000, -0.029942, -0.029200, +0.020000, +0.036364/)
Type(BG) :: field
Type(Pert)::pertur

interface 
	Function func1(i,x,yi,ydi,field,pertur)
	use params
	integer :: i
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG) :: field
	Type(Pert) :: pertur
	end function func1

	Function func2(i,x,yi,ydi,field,pertur)
	use params
	integer :: i	
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG) :: field
	Type(Pert) :: pertur	

	end function func2
end interface

	do i = 1, 5, 1
		x(i) = xi + h * c(i)
	end do

	k(1) = func1(j, xi, yi, ydi, field ,pertur)
	l(1) = func2(j, xi, yi, ydi, field ,pertur)

		y = yi + h * a(5*0+1) * k(1)
		yd = ydi + h * a(5*0+1) * l(1)
	k(2) = func1(j, x(1), y, yd, field ,pertur)
	l(2) = func2(j, x(1), y, yd, field ,pertur)

		y = yi + h * (a(5*1+1) * k(1) + a(5*1+2) * k(2))
		yd = ydi + h * (a(5*1+1) * l(1) + a(5*1+2) * l(2))
	k(3) = func1(j, x(2), y, yd, field ,pertur)
	l(3) = func2(j, x(2), y, yd, field ,pertur)

		y = yi + h * (a(5*2+1) * k(1) + a(5*2+2) * k(2) + a(5*2+3) * k(3))
		yd = ydi + h * (a(5*2+1) * l(1) + a(5*2+2) * l(2) + a(5*2+3) * l(3))
	k(4) = func1(j, x(3), y, yd, field ,pertur)
	l(4) = func2(j, x(3), y, yd, field ,pertur)


		y = yi + h * (a(5*3+1) * k(1) + a(5*3+2) * k(2) + a(5*3+3) * k(3) + a(5*3+4) * k(4))
		yd = ydi + h * (a(5*3+1) * l(1) + a(5*3+2) * l(2) + a(5*3+3) * l(3) + a(5*3+4) * l(4))
	k(5) = func1(j, x(4), y, yd, field ,pertur)
	l(5) = func2(j, x(4), y, yd, field ,pertur)

		y = yi + h * (a(5*4+1) * k(1) + a(5*4+2) * k(2) + a(5*4+3) * k(3) + a(5*4+4) * k(4) + a(5*4+5) * k(5))
		yd = ydi + h * (a(5*4+1) * l(1) + a(5*4+2) * l(2) + a(5*4+3) * l(3) + a(5*4+4) * l(4) + a(5*4+5) * l(5))  
	k(6) = func1(j, x(5), y, yd, field ,pertur)
	l(6) = func2(j, x(5), y, yd, field ,pertur)

		y = yi + h * (b5(1) * k(1) + b5(2) * k(2) + b5(3) * k(3) + b5(4) * k(4) + b5(5) * k(5) + b5(6) * k(6))
		yd = ydi + h * (b5(1) * l(1) + b5(2) * l(2) + b5(3) * l(3) + b5(4) * l(4) + b5(5) * l(5) + b5(6) * l(6))

	yi = y
	ydi = yd
	
	e1 = 0
	e2 = 0
		e1 = abs(h * (b45(1) * k(1) + b45(2) * k(2) + b45(3) * k(3) + b45(4) * k(4) + b45(5) * k(5) + b45(6) * k(6)) &
			& - h * (b5(1) * k(1) + b5(2) * k(2) + b5(3) * k(3) + b5(4) * k(4) + b5(5) * k(5) + b5(6) * k(6)))	 	! error in y 
		e1 = e1/abs(y)
!		e2 = abs(h * (b45(1) * l(1) + b45(2) * l(2) + b45(3) * l(3) + b45(4) * l(4) + b45(5) * l(5) + b45(6) * l(6)) &
!			  & - h * (b5(1) * l(1) + b5(2) * l(2) + b5(3) * l(3) + b5(4) * l(4) + b5(5) * l(5) + b5(6) * l(6))) 		! + error in yd = error in both
!		e2 = e2/abs(yd)
		er = e1+e2

end Subroutine RKF5
!----------------------------------------------------------------------------------------------------------------------------------------------
Subroutine RKCK5(j,xi,h,yi,ydi,z,zd,func1,func2,field,pertur,er)

Integer :: i, j, i1, i2, i3, i4, i5
Double Precision :: xi, h, e1, e2, er
Complex*16 :: yi, ydi 
Complex*16 :: func1, func2
Complex*16 :: y, yd, z, zd
Complex*16, dimension (6) :: k, l
COMPLEX*16 :: e
Double precision, parameter :: a16 = -1.1d1/5.4d1, a18 = -7.0d1/2.7d1, a19 = +3.5d1/2.7d1, a21 = 1.631d3/5.5296d4, &
			     & a22 = 1.75d2/5.12d2, a23 = +5.75d2/1.3824d4, a24 = +4.4275d4/1.10592d5, a25 = +2.53d2/4.096d3
Double precision, parameter :: b51 = +3.7d1/3.78d2,  b53 = +2.50d2/6.21d2, b54 = +1.25d2/5.94d2, b56 = +5.12d2/1.771d3
Double precision, parameter :: b451 = +2.825d3/2.7648d4, b453 = +1.8575d4/4.8384d4, b454 = +1.3525d4/5.5296d4, &
			     & b455 = +2.77d2/1.4336d4

DOUBLE PRECISION, dimension(5):: c = (/+0.2d0, +0.3d0, +0.6d0, +1.0d0, +0.875d0/), x
DOUBLE PRECISION, dimension(25) :: a = (/ +0.2d0, +0.0d0, +0.0d0, +0.0d0, +0.0d0, &
					& +0.075d0, +0.225d0, +0.0d0, +0.0d0, +0.0d0, &
					& +0.3d0, -0.9d0, +1.2d0, +0.0d0, +0.0d0, &			! check ordering
					& a16, +2.5d0, a18, a19, +0.0d0, &
					& a21, a22, a23, a24, a25 /)

DOUBLE PRECISION, dimension(6) :: b5 = (/b51, +0.0d0, b53, b54, +0.0d0, b56/)
DOUBLE PRECISION, dimension(6) :: b45= (/b451, +0.0d0, b453, b454, b455, +0.25d0/)

Type(BG) :: field
Type(Pert)::pertur

interface 
	Function func1(i,x,yi,ydi,field,pertur)
	use params
	integer :: i
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG) :: field
	Type(Pert) :: pertur
	end function func1

	Function func2(i,x,yi,ydi,field,pertur)
	use params
	integer :: i	
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG) :: field
	Type(Pert) :: pertur	

	end function func2
end interface

	do i = 1, 5, 1
		x(i) = xi + h * c(i)
	end do

	k(1) = func1(j, xi, yi, ydi, field ,pertur)
	l(1) = func2(j, xi, yi, ydi, field ,pertur)

		i1 = 5*0+1
		y = yi + h * a(i1) * k(1)
		yd = ydi + h * a(i1) * l(1)
	k(2) = func1(j, x(1), y, yd, field ,pertur)
	l(2) = func2(j, x(1), y, yd, field ,pertur)

		i1 = 5*1+1
		i2 = 5*1+2
		y = yi + h * (a(i1) * k(1) + a(i2) * k(2))
		yd = ydi + h * (a(i1) * l(1) + a(i2) * l(2))
	k(3) = func1(j, x(2), y, yd, field ,pertur)
	l(3) = func2(j, x(2), y, yd, field ,pertur)

		i1 = 5*2+1
		i2 = 5*2+2
		i3 = 5*2+3
		y = yi + h * (a(i1) * k(1) + a(i2) * k(2) + a(i3) * k(3))
		yd = ydi + h * (a(i1) * l(1) + a(i2) * l(2) + a(i3) * l(3))
	k(4) = func1(j, x(3), y, yd, field ,pertur)
	l(4) = func2(j, x(3), y, yd, field ,pertur)

		i1 = 5*3+1
		i2 = 5*3+2
		i3 = 5*3+3
		i4 = 5*3+4
		y = yi + h * (a(i1) * k(1) + a(i2) * k(2) + a(i3) * k(3) + a(i4) * k(4))
		yd = ydi + h * (a(i1) * l(1) + a(i2) * l(2) + a(i3) * l(3) + a(i4) * l(4))
	k(5) = func1(j, x(4), y, yd, field ,pertur)
	l(5) = func2(j, x(4), y, yd, field ,pertur)

		i1 = 5*4+1
		i2 = 5*4+2
		i3 = 5*4+3
		i4 = 5*4+4
		i5 = 5*4+5
		y = yi + h * (a(i1) * k(1) + a(i2) * k(2) + a(i3) * k(3) + a(i4) * k(4) + a(i5) * k(5))
		yd = ydi + h * (a(i1) * l(1) + a(i2) * l(2) + a(i3) * l(3) + a(i4) * l(4) + a(i5) * l(5))  
	k(6) = func1(j, x(5), y, yd, field ,pertur)
	l(6) = func2(j, x(5), y, yd, field ,pertur)

		y = yi + h * (b5(1) * k(1) + b5(2) * k(2) + b5(3) * k(3) + b5(4) * k(4) + b5(5) * k(5) + b5(6) * k(6))
		yd = ydi + h * (b5(1) * l(1) + b5(2) * l(2) + b5(3) * l(3) + b5(4) * l(4) + b5(5) * l(5) + b5(6) * l(6))

!	yi = y
!	ydi = yd

	z = y
	zd = yd

	e1 = 0
	e2 = 0
		e1 = abs(h * (b45(1) * k(1) + b45(2) * k(2) + b45(3) * k(3) + b45(4) * k(4) + b45(5) * k(5) + b45(6) * k(6)) &
			& - h * (b5(1) * k(1) + b5(2) * k(2) + b5(3) * k(3) + b5(4) * k(4) + b5(5) * k(5) + b5(6) * k(6)))	 	! error in y 
		e1 = e1/abs(y)
		e2 = abs(h * (b45(1) * l(1) + b45(2) * l(2) + b45(3) * l(3) + b45(4) * l(4) + b45(5) * l(5) + b45(6) * l(6)) &
			  & - h * (b5(1) * l(1) + b5(2) * l(2) + b5(3) * l(3) + b5(4) * l(4) + b5(5) * l(5) + b5(6) * l(6))) 		! + error in yd = error in both
		e2 = e2/abs(yd)
		er = e1+e2

		if (er .ge. 0.0d0) then
			continue
		else if (er .le. 0.0d0) then
			continue
		else 
		print*, xi, yi, ydi, k
		print*, "error accumulation in RK routine ! stopping !!"
		stop
		end if

end Subroutine RKCK5
!----------------------------------------------------------------------------------------------------------------------------------------------
Subroutine RKDP5(j,xi,h,yi,ydi,func1,func2,field,pertur,er)						! TO DO 

Integer :: i, j
Double Precision :: xi, h, e1, e2, er
Complex*16 :: yi, ydi 
Complex*16 :: func1, func2
Complex*16 :: y, yd
Complex*16, dimension (7) :: k, l
COMPLEX*16 :: e

Double precision, parameter :: c4 = 8.0d0/9.0d0

Double precision, parameter :: a13 = +4.4d1/4.5d1, a14 = -5.6d1/1.5d1, a15 = +3.2d1/9.0d0, &
			     & a19 = +1.9372d4/6.561d3, a20 = -2.5360d4/2.187d3, a21 = +6.4448d4/6.561d3, &
			     & a22 = -2.12d2/7.29d2, a25 = +9.017d3/3.168d3, a26 = -3.55d2/3.3d1, &
			     & a27 = +4.6732d4/5.247d3, a28 = +4.9d1/1.76d2, a29 = -5.103d3/1.8656d4, &
			     & a31 = +3.5d1/3.84d2, a33 = +5.0d2/1.113d3, a34 = +1.25d2/1.92d2, &
			     & a35 = -2.187d3/6.784d3, a36 = +1.1d1/8.4d1

Double precision, parameter :: b51 = +3.5d1/3.84d2,  b53 = +5.0d2/1.113d3, b54 = +1.25d2/1.92d2, &
			     & b55 = -2.187d3/6.784d3, b56 = +1.1d1/8.4d1

Double precision, parameter :: b451 = +5.179d3/5.76d4, b453 = +7.571d3/1.6695d4, b454 = +3.93d2/6.40d2, &
			     & b455 = -9.2097d4/3.392d5, b456 = +1.87d2/2.1d3, b457 = +0.025d0

DOUBLE PRECISION, dimension(6):: c = (/+0.2d0, +0.3d0, +0.8d0, c4, 1.0d0, 1.0d0/), x

DOUBLE PRECISION, dimension(36) :: a = (/ +0.2d0, +0.0d0, +0.0d0, +0.0d0, +0.0d0, +0.0d0, &
					& +0.075d0, +0.225d0, +0.0d0, +0.0d0, +0.0d0, +0.0d0, &
					& a13, a14, a15, +0.0d0, +0.0d0, +0.0d0, &			! check ordering
					& a19, a20, a21, a22, +0.0d0, +0.0d0,&
					& a25, a26, a27, a28, a29, +0.0d0, &
					& a31, 0.0d0, a33, a34, a35, a36 /)

DOUBLE PRECISION, dimension(7) :: b5 = (/b51, +0.0d0, b53, b54, b55, b56, 0.0d0/)
DOUBLE PRECISION, dimension(7) :: b45= (/b451, +0.0d0, b453, b454, b455, b456, b457/)

Type(BG) :: field
Type(Pert)::pertur

interface 
	Function func1(i,x,yi,ydi,field,pertur)
	use params
	integer :: i
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG) :: field
	Type(Pert) :: pertur
	end function func1

	Function func2(i,x,yi,ydi,field,pertur)
	use params
	integer :: i	
	double precision :: x
	Complex*16 :: yi, ydi
	Type(BG) :: field
	Type(Pert) :: pertur	

	end function func2
end interface

	do i = 1, 6, 1
		x(i) = xi + h * c(i)
	end do

	k(1) = func1(j, xi, yi, ydi, field ,pertur)
	l(1) = func2(j, xi, yi, ydi, field ,pertur)

		y = yi + h * a(6*0+1) * k(1)
		yd = ydi + h * a(6*0+1) * l(1)
	k(2) = func1(j, x(1), y, yd, field ,pertur)
	l(2) = func2(j, x(1), y, yd, field ,pertur)

		y = yi + h * (a(6*1+1) * k(1) + a(6*1+2) * k(2))
		yd = ydi + h * (a(6*1+1) * l(1) + a(6*1+2) * l(2))
	k(3) = func1(j, x(2), y, yd, field ,pertur)
	l(3) = func2(j, x(2), y, yd, field ,pertur)

		y = yi + h * (a(6*2+1) * k(1) + a(6*2+2) * k(2) + a(6*2+3) * k(3))
		yd = ydi + h * (a(6*2+1) * l(1) + a(6*2+2) * l(2) + a(6*2+3) * l(3))
	k(4) = func1(j, x(3), y, yd, field ,pertur)
	l(4) = func2(j, x(3), y, yd, field ,pertur)


		y = yi + h * (a(6*3+1) * k(1) + a(6*3+2) * k(2) + a(6*3+3) * k(3) + a(6*3+4) * k(4))
		yd = ydi + h * (a(6*3+1) * l(1) + a(6*3+2) * l(2) + a(6*3+3) * l(3) + a(6*3+4) * l(4))
	k(5) = func1(j, x(4), y, yd, field ,pertur)
	l(5) = func2(j, x(4), y, yd, field ,pertur)

		y = yi + h * (a(6*4+1) * k(1) + a(6*4+2) * k(2) + a(6*4+3) * k(3) + a(6*4+4) * k(4) + a(6*4+5) * k(5))
		yd = ydi + h * (a(6*4+1) * l(1) + a(6*4+2) * l(2) + a(6*4+3) * l(3) + a(6*4+4) * l(4) + a(6*4+5) * l(5))  
	k(6) = func1(j, x(5), y, yd, field ,pertur)
	l(6) = func2(j, x(5), y, yd, field ,pertur)

		y = yi + h * (a(6*5+1) * k(1) + a(6*5+2) * k(2) + a(6*5+3) * k(3) + a(6*5+4) * k(4) + a(6*5+5) * k(5) + a(6*5+6) * k(6))
		yd = ydi + h * (a(6*5+1) * l(1) + a(6*5+2) * l(2) + a(6*5+3) * l(3) + a(6*5+4) * l(4) + a(6*5+5) * l(5) + a(6*5+6) * l(6))  
	k(7) = func1(j, x(6), y, yd, field ,pertur)
	l(7) = func2(j, x(6), y, yd, field ,pertur)

		y = yi + h * (b5(1) * k(1) + b5(2) * k(2) + b5(3) * k(3) + b5(4) * k(4) + b5(5) * k(5) + b5(6) * k(6) + b5(7) * k(7))
		yd = ydi + h * (b5(1) * l(1) + b5(2) * l(2) + b5(3) * l(3) + b5(4) * l(4) + b5(5) * l(5) + b5(6) * l(6) + b5(7) * l(7))

	yi = y
	ydi = yd
	
	e1 = 0
	e2 = 0
		e1 = abs(h * (b45(1) * k(1) + b45(2) * k(2) + b45(3) * k(3) + b45(4) * k(4) + b45(5) * k(5) + b45(6) * k(6) + b45(7) * k(7)) &
			& - h * (b5(1) * k(1) + b5(2) * k(2) + b5(3) * k(3) + b5(4) * k(4) + b5(5) * k(5) + b5(6) * k(6) + b5(7) * k(7)))	 	! error in y 
		e1 = e1/abs(y)
		e2 = abs(h * (b45(1) * l(1) + b45(2) * l(2) + b45(3) * l(3) + b45(4) * l(4) + b45(5) * l(5) + b45(6) * l(6) + b45(7) * l(7)) &
			  & - h * (b5(1) * l(1) + b5(2) * l(2) + b5(3) * l(3) + b5(4) * l(4) + b5(5) * l(5) + b5(6) * l(6)) + b5(7) * l(7)) 		! + error in yd = error in both
		e2 = e2/abs(yd)
		er = e1+e2

end Subroutine RKDP5

!----------------------------------------------------------------------------------------------------------------------------------------------

Recursive subroutine RKadap(i,e,s,p,pd,func1,func2,Field1,ACurv,yerr)

Integer :: i
Double precision :: e, s, yerr
Complex*16 :: p, pd, z, zd
Type(BG) :: Field1
Type(Pert) :: ACurv

interface 
	Complex*16 Function func1(i,e,p,pd,field,pertur)
	use params
	integer :: i
	double precision :: e
	Complex*16 :: p, pd
	Type(BG) :: field
	Type(Pert) :: pertur
	end function func1

	Complex*16 Function func2(i,e,p,pd,field,pertur)
	use params
	integer :: i	
	double precision :: e
	Complex*16 :: p, pd
	Type(BG) :: field
	Type(Pert) :: pertur	
	end function func2
end interface

!	Call RKF5(i,e,s,p,pd,func1,func2,Field1,ACurv,yerr)
!	Call RKCK5(i,e,s,p,pd,func1,func2,Field1,ACurv,yerr)
!	Call RKDP5(i,e,s,p,pd,func1,func2,Field1,ACurv,yerr)

	Call RKCK5(i,e,s,p,pd,z,zd,func1,func2,Field1,ACurv,yerr)

		if (yerr .ge. toler) then
			if (s .ge. 1.0d-32) then 
				s = s/2.0d0
				Call RKadap(i,e,s,p,pd,func1,func2,Field1,ACurv,yerr)		! adaptive call with reduced step size.
			else
				print*
				print*, "minimum step size reached"
				return
			end if

		else if (yerr .lt. toler) then				
			if (s .lt. 1.0d0/DBLE(NOS*Nadap)) then
				s = 2.0d0*s	
!!				s = (1.0d0/DBLE(NOS))
				Call RKadap(i,e,s,p,pd,func1,func2,Field1,ACurv,yerr)
!!				return
			else
				p = z
				pd = zd
!!				return
			end if
		else
			print*, "error =", yerr
			return
		end if

End subroutine RKadap
!----------------------------------------------------------------------------------------------------------------------------------------------
Subroutine Integ(i,x,dx,grand,gral,fi,ac1,ac2,ac3,ka)

Integer :: i,i1,i2,i3,i4,i5
Double Precision :: s1,s2,s3,s4,s5,ka
Double precision :: x,dx,x1,x2,x3,x4,x5
Complex*16 :: grand,gral,error

Type(BG) :: fi
Type(Pert)::ac1,ac2,ac3

interface
	function grand(i,fi,ac1,ac2,ac3,ka)
	use params
	integer :: i	
	Complex*16 :: yi, ydi
	double precision :: ka
	Type(BG) :: fi
	Type(Pert) :: ac1,ac2,ac3
	end function
end interface

i1 = i
i2 = i+1
i3 = i+2
i4 = i+3
i5 = i+4

!s1 = 7.0d0*fi%phiN(1,i1+1)					! step size corr. to index i by Boole's rule ?check?
!s2 = 32.0d0*fi%phiN(1,i2+1)
!s3 = 12.0d0*fi%phiN(1,i3+1)
!s4 = 32.0d0*fi%phiN(1,i4+1)
!s5 = 7.0d0*fi%phiN(1,i5+1)

s1 = fi%estep(i1+1)						! step size corr. to index i ?check?
s2 = fi%estep(i2+1)
s3 = fi%estep(i3+1)
s4 = fi%estep(i4+1)
s5 = fi%estep(i5+1)

!gral = (2.0d0/45.0d0)*(s1*grand(i1,fi,ac1,ac2,ac3)+s2*grand(i2,fi,ac1,ac2,ac3) &
!	& + s3*grand(i3,fi,ac1,ac2,ac3)+s4*grand(i4,fi,ac1,ac2,ac3)+s5*grand(i5,fi,ac1,ac2,ac3))	! Ref. Boole's rule (Bode's rule in Abramowitz and Stegun)

gral = (1.0d0/3.0d0)*(s1*grand(i1,fi,ac1,ac2,ac3,ka) + 4.0d0*s2*grand(i2,fi,ac1,ac2,ac3,ka) &
	& + 2.0d0*s3*grand(i3,fi,ac1,ac2,ac3,ka) + 4.0d0*s4*grand(i4,fi,ac1,ac2,ac3,ka) + s5*grand(i5,fi,ac1,ac2,ac3,ka))

error = -8.0d0*(dx**7)*grand(i,fi,ac1,ac2,ac3,ka)/(9.45d2)						! can be calculated anywhere b/w x1-x5

End subroutine Integ

!----------------------------------------------------------------------------------------------------------------------------------------------
END MODULE RK_45
!----------------------------------------------------------------------------------------------------------------------------------------------
