SHELL=/bin/sh

SRCS= \
Params.f90\
HV.f90\
RK_45.f90\
PBS.f90\

OBJS= \
Params.o\
HV.o\
RK_45.o\
PBS.o\

MODS= \
params.mod\
hv.mod\
rk_45.mod\

HDRS= 

PLT=\
plot.plt

MAKEFILE= Makefile

COMMAND=  PBS
 
CC= gfortran
CFLAGS=  -g -O3 -fopenmp
WARNFLAGS= 
LDFLAGS= -lm 

$(COMMAND): $(OBJS) 
	$(CC) -o $(COMMAND) -O3 -fopenmp $(OBJS) $(LDFLAGS) $(LIBS)
        	 
Params.o Params.mod : Params.f90
	$(CC) $(CFLAGS) $(WARNFLAGS) -c Params.f90 -o Params.o

HV.mod HV.o : HV.f90
	$(CC) $(CFLAGS) $(WARNFLAGS) -c HV.f90 -o HV.o

rk_45.mod RK_45.o : RK_45.f90
	$(CC) $(CFLAGS) $(WARNFLAGS) -c RK_45.f90 -o RK_45.o

PBS.o : PBS.f90
	$(CC) $(CFLAGS) $(WARNFLAGS) -c PBS.f90 -o PBS.o

run:
	time ./PBS > verbose.out 2>&1
	echo "run complete"
plot:
	gnuplot 'plot.plt'
	echo "plot files generated"
clean:
	rm -f $(OBJS) $(MODS)

all:
	make clean; make; make run; make plot

delete:
	rm -f $(OBJS) $(MODS) $(COMMAND) *txt *pdf *out
tarz:
	tar zcf - $(MAKEFILE) $(SRCS) $(HDRS) $(PLT) > $(COMMAND).tar.gz
