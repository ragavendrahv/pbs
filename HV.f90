Module HV 		! potential and H evaluation

Use Params

Implicit None

Double precision, parameter ::  c1 = 8.8738d-2, &
				c2 = -0.401000d0, &					!testing
				c3 = 1.0d0 + 2.99d-4, &
				c4 = 3.62037d0
Contains 

FUNCTION V(phi,paramets)					! function whose first value is potential(V) and second is derivative of potential w.r.t. phi (V_phi)
Double Precision :: logm, m, phi, phi_0,  phi_r, lambda
Double Precision ::  g, gx, gxx
Double Precision, dimension(2) :: paramets
Double precision, dimension(3) :: V, Vpart1, Vpart2, stepper, bumper, dipper
Double precision :: b, d, V0, A1, A2, Asum, Adiff, del_phi, t1, t2, t3
Double precision :: c0, c1, c2, c3, c4, V1, phi_r1, phi_r2
Double precision :: coupler, phi_step, phi_bump, phi_dip, phi_width

!----------------------------------------------------------------------------------------------------------------------------------------------
!m = paramets(1) 						! identifying the parameters received 
!phi_0 = paramets(2)

!phi_r = phi/phi_0

!V(1) = (m*phi)**2/2 - 2*(m*phi_0)**2*(phi_r)**n/n + (m*phi_0**2/phi)**2*(phi_r)**(2*n)/(2*(n-1))	! RIPI
!V(2) = m**2*phi - 2.0d0*m**2*phi_r**(n-1)*phi_0 + m**2*phi_0*phi_r**(2.0d0*n-3)					! RIPI
!V(3) = m**2*(1.0d0-2.0d0*(n-1)*phi_r**(n-2)+(2.0d0*n-3)*phi_r**(2.0d0*n-4))						! RIPI
!----------------------------------------------------------------------------------------------------------------------------------------------
logm = paramets(1) 						! identifying the parameters received 
phi_0 = paramets(2)

m = (10**logm)*1.0d-10
m = sqrt(m)									! param2 = log10(m*m*10^10)

V(1) = (m*phi)*(m*phi)/2.0d0 					! quadratic
V(2) = m*m*phi
V(3) = m*m
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m									! Staro
!phi_r = phi/phi_0

!V(1) = V0*((1.0d0 - dexp(-phi_r))**2)/(8.0d0)				! Staro
!V(2) = V0*(dexp(-phi_r) - dexp(-2.0d0*phi_r))/(4.0d0*phi_0)			! Staro
!V(3) = V0*(2.0d0*dexp(-2.0d0*phi_r) - dexp(-phi_r))/(4.0d0*phi_0*phi_0)	! Staro
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m
!del_phi = phi_0/1.0d4						! Staro-II, Ref.: Martin, Sriram
!phi_r = (phi-phi_0)/del_phi

!A1 = 3.35d-14
!A2 = 7.26d-15						! Ref.: Martin, et.al.

!A1 = (1.743524d-1)*V0		! 4.35881d-10					! best fit
!A2 = (1.0d-1)*V0		! 2.49900d-10					! best fit

!Asum = A1+A2
!Adiff = A1-A2

!V(1) = V0 + del_phi*(Asum)*(phi_r)/2.0d0 + Adiff*del_phi*phi_r*tanh(phi_r)/2.0d0 			! Staro-II				
!V(2) = (Asum + Adiff*tanh(phi_r) + (Adiff*phi_r)/(cosh(phi_r)*cosh(phi_r)) )/2.0d0			! Staro-II
!V(3) = (Adiff)*(1.0d0 - phi_r*tanh(phi_r))/(del_phi*cosh(phi_r)*cosh(phi_r))				! Staro-II
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m**4
!phi_r = phi/phi_0

!V(1) = V0*(1.0d0 - phi_r**4)					! small field
!V(2) = -4.0d0*V0*(phi_r**3)/(phi_0)
!V(3) = -12.0d0*V0*(phi_r**2)/(phi_0**2)
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m
!phi_r = phi/phi_0

!! c1 = 1.0d0				! old value
!! c1 = 0.55d0				! against mathematica notebook
! c1 = 0.552d0				! new value

!V(1) = V0*(1.0d0 + c1*phi_r**4)					! Roberts et.al.
!V(2) = 4.0d0*V0*c1*(phi_r**3)/(phi_0)				! Roberts et.al.
!V(3) = 12.0d0*V0*c1*(phi_r**2)/(phi_0**2)			! Roberts et.al.
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m**3							! \mu^3 in Axion
!phi_r = phi/phi_0

!b = 1.063d-2							! for Axion from DC et.al.
!d = 0.0d0							! for Axion from DC et.al.

!b = 0.408571 
!b = (1.84d-4)/phi_0				! from BINGO
!d = 0.336

!V(1) = V0*(phi+b*phi_0*cos(phi_r + d))				! Axion Monodromy ref. Debika, et.al.
!V(2) = V0*(1-b*sin(phi_r + d))					! Axion Monodromy
!V(3) = -V0*b*cos(phi_r + d)/phi_0				! Axion Monodromy
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m**3							! small field Axion
!phi_r = phi/phi_0

!b = 1.0d-2
!d = PI
! c1 = 1.0d-1

!V(1) = V0*(2.0d0 - c1*phi + b*phi_0*cos(phi_r + d))
!V(2) = -V0*(c1+b*sin(phi_r + d))
!V(3) = -V0*b*cos(phi_r + d)/phi_0
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m
!phi_r = phi/phi_0								! SFPI
! c1 = 0.0d0
! c2 = 2.061d-10
! c3 = 1.165d-10								! Ref. Arindam, et.al.
! c4 = 2.248d-11

!	V(1) = V0*(20.0d0 - c1*phi_r + c2*phi_r**2 - c3*phi_r**3 + c4*phi_r**4)
!	V(2) = V0*(-c1 + 2.0*c2*phi_r - 3.0*c3*phi_r**2 + 4.0*c4*phi_r**3)/phi_0	! SFPI
!	V(3) = V0*(2.0*c2 - 6.0*c3*phi_r + 12.0*c4*phi_r**2)/(phi_0*phi_0)

!	V(1) = V0 + c1*phi_r + c2*phi_r**2 - c3*phi_r**3 + c4*phi_r**4
!	V(2) = (c1 + 2.0*c2*phi_r - 3.0*c3*phi_r**2 + 4.0*c4*phi_r**3)/phi_0	! SFPI
!	V(3) = (2.0*c2 - 6.0*c3*phi_r + 12.0*c4*phi_r**2)/(phi_0*phi_0)

!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m**3
!phi_r = (phi-1.0d0)/phi_0								! SFPI

!	V(1) = V0*(2.0d0*phi - phi_r**3)		! testing
!	V(2) = V0*(2.0d0*phi_0 - 3.0d0*phi_r**2)/phi_0							! SFPI
!	V(3) = V0*(-6.0d0*phi_r)/(phi_0*phi_0)
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m
!phi_r = phi/phi_0

! c1 = 8.5220045435404804E-002  
! c2 = -0.46899167535097835  
! c3 = 1.0000000000000000  
! c4 = 1.5092819918189926  

! c1 = 8.53E-002  
! c2 = -0.458d0  							! Bhaumik et.al. modified
! c3 = 1.0000000000000000  
! c4 = 1.5092d0  
									! Bhaumik et.al. ref.: 1907.04125
! c1 = 7.2450312039055734E-002
! c2 = -0.43512722391552722
! c3 = 1.0000000000000000
! c4 = 1.5017550848670957  

!Vpart1(1) = c1*phi_r**2 + c2*phi_r**4 + c3*phi_r**6
!Vpart1(2) = 2.0d0*c1*phi_r + 4.0d0*c2*phi_r**3 + 6.0d0*c3*phi_r**5
!Vpart1(3) = 2.0d0*c1 + 12.0d0*c2*phi_r**2 + 30.0d0*c3*phi_r**4

!Vpart2(1) = 1.0d0/(1.0d0 + c4*phi_r**2)**2
!Vpart2(2) = -4.0d0*c4*phi_r/(1.0d0 + c4*phi_r**2)**3
!Vpart2(3) = -4.0d0*c4/(1.0d0 + c4*phi_r**2)**3 + 24.0d0*c4*c4*phi_r*phi_r/(1.0d0 + c4*phi_r**2)**4

!V(1) = V0*Vpart1(1)*Vpart2(1)
!V(2) = V0*(Vpart1(2)*Vpart2(1) + Vpart1(1)*Vpart2(2))/phi_0
!V(3) = V0*(Vpart1(3)*Vpart2(1) + 2.0d0*Vpart1(2)*Vpart2(2) + Vpart1(1)*Vpart2(3))/phi_0**2

!!V(1) = V0*(c1*phi_r**2 + c2*phi_r**4 + c3*phi_r**6)/((1.0d0 + c4*phi_r**2 )**2)
!!V(2) = 2.0d0*V0*(c1*phi_r + (2.0d0*c2-c1*c4)*phi_r**3 + c3*phi_r**5 + c3*c4*phi_r**7)/((phi_0)*(1.0d0 + c4*phi_r**2)**3)
!!V(3) = 2.0d0*V0*(c1 + 2.0d0*(3.0d0*c2-4.0d0*c1*c4)*phi_r**2 + (5.0d0*c3 + 3.0d0*c1*c4*c4 - 6.0d0*c2*c4)*phi_r**4 + &
!!	& 6.0d0*c3*c4*phi_r**6 + c3*c4*c4*phi_r**8)/((phi_0*phi_0)*(1.0d0 + c4*phi_r**2)**4)
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m
!phi_r = phi/phi_0

! c1 = 1.0d0
!! c2 = 1.0d0 - c1*c1/3.0d0 + c1*c1*((9.0d0/(2.0d0*c1*c1) - 1.0d0)**(2.0d0/3.0d0))/3.0d0 - 1.0d-4
! c2 = 1.435d0 - 1.0d-4

!alternative set
!V0 = m*phi_0**4
! c1 = 1.0d0/sqrt(2.0d0)				! Garcia Bellido - reply ref.: 1706.04226
! c2 = 1.5d0
													! Garcia Bellido ref.: 1702.03901
!Vpart1(1) = 6.0d0*phi_r**2 - 4.0d0*c1*phi_r**3 + 3.0d0*phi_r**4
!Vpart1(2) = 12.0d0*phi_r - 12.0d0*c1*phi_r**2 + 12.0d0*phi_r**3
!Vpart1(3) = 12.0d0 - 24.0d0*c1*phi_r + 36.0d0*phi_r**2

!Vpart2(1) = 1.0d0/(1.0d0 + c2*phi_r**2)**2
!Vpart2(2) = -4.0d0*c2*phi_r/(1.0d0 + c2*phi_r**2)**3
!Vpart2(3) = -4.0d0*c2/(1.0d0 + c2*phi_r**2)**3 + 24.0d0*c2*c2*phi_r*phi_r/(1.0d0 + c2*phi_r**2)**4

!V(1) = V0*Vpart1(1)*Vpart2(1)
!V(2) = V0*(Vpart1(2)*Vpart2(1) + Vpart1(1)*Vpart2(2))/phi_0
!V(3) = V0*(Vpart1(3)*Vpart2(1) + 2.0d0*Vpart1(2)*Vpart2(2) + Vpart1(1)*Vpart2(3))/phi_0**2
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m							! alpha-attractor
!phi_r = phi/phi_0

!V(1) = V0*((1-exp(-phi_r))**2)				! alpha-attractor
!V(2) = 2.0d0*V0*(exp(-phi_r)-exp(-2*phi_r))/phi_0		! alpha-attractor
!V(3) = 2.0d0*V0*(2.0d0*exp(-2.0d0*phi_r)-exp(-phi_r))/(phi_0*phi_0)	! alpha-attractor
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m
!phi_r = phi/(phi_0)

! c0 = 0.16401d0
! c1 = 0.3d0
! c2 = -1.426d0							!Dalianis, et.al. Fig.7 I_1
! c3 = 2.20313d0

!t1 = (c0 + c1*tanh(phi_r) + c2*tanh(phi_r)*tanh(phi_r) + c3*tanh(phi_r)*tanh(phi_r)*tanh(phi_r))
!t2 = (c1/(cosh(phi_r)*cosh(phi_r)) + 2.0d0*c2*tanh(phi_r)/(cosh(phi_r)*cosh(phi_r)) &
!	& + 3.0d0*c3*tanh(phi_r)*tanh(phi_r)/(cosh(phi_r)*cosh(phi_r)))

!t3 = -2.0d0*c1*tanh(phi_r)/(cosh(phi_r)**2) + 2.0d0*c2/cosh(phi_r)**4 &
!	& - 4.0d0*c2*(tanh(phi_r)**2)/(cosh(phi_r)**2) &
!	& + 6.0d0*c3*tanh(phi_r)/cosh(phi_r)**4 &
!	& - 6.0d0*c3*(tanh(phi_r)**3)/cosh(phi_r)**2

!V(1) = V0*t1*t1							!alpha-attractor as in 1805.09483
!V(2) = 2.0d0*V0*t1*t2/phi_0
!V(3) = 2.0d0*V0*(t2*t2 + t1*t3)/(phi_0*phi_0)
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m
!phi_r = phi/(phi_0)

! c0 = tanh(phi_r)

! c1 = 0.130383d0
! c2 = 0.129576d0							!Dalianis - Model II Table 2

!t1 = c0 + c1*sin(c0/c2)
!t2 = (1.0d0 + c1*cos(c0/c2)/c2)/(cosh(phi_r)*cosh(phi_r))
!t3 = 2.0d0*c0/(cosh(phi_r)**2) + c1*(sin(c0/c2)/cosh(phi_r)**4)/(c2**2) &
!	+ 2.0d0*c0*c1*cos(c0/c2)/(c2*cosh(phi_r)**2)

!V(1) = V0*t1*t1								!alpha-attractor as in 1805.09483
!V(2) = 2.0d0*V0*(t1*t2)/phi_0
!V(3) = 2.0d0*V0*(t2*t2 - t1*t3)/(phi_0*phi_0)
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m**4
!V1 = (PI**4)/8.0d0
!V1 = 1.3d0*V0
! c1 = PI/4.0d0
! c2 = (1.0d0-1.0d-4)
! c3 = 2.0d0

!phi_r1 = phi/phi_0
!phi_r2 = c3*phi/phi_0

!V(1) = V0*(cos(phi_r1+c1) - (c2/(c3*c3))*cos(phi_r2)) + V1				! HTPI
!V(2) = V0*(-sin(phi_r1+c1) + (c2/c3)*sin(phi_r2))/phi_0				! HTPI
!V(3) = V0*(-cos(phi_r1+c1) + (c2)*cos(phi_r2))/(phi_0*phi_0)				! HTPI
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = 1.0d-6
!phi_r = phi/phi_0	
!	V(1) = V0/(cosh(phi_r))
!	V(2) = -V0*tanh(phi_r)/(phi_0*cosh(phi_r))				! Tachyon - I
!	V(3) = V0*(2.0d0*tanh(phi_r)*tanh(phi_r)-1.0d0)/(phi_0*phi_0*cosh(phi_r))
!----------------------------------------------------------------------------------------------------------------------------------------------
!V0 = m*m							! modified Starobinsky as in 1305.1247
!phi_r = phi/phi_0
! c1 = (1.0d0/3.0d0)
! c1 = 3.3310d-1

!V(1) = V0*sinh(phi_r)*sinh(phi_r)*(cosh(phi_r) - 3.0d0*c1*sinh(phi_r))**2
!V(2) = 2.0d0*V0*sinh(phi_r)*(-cosh(phi_r) + 3.0d0*c1*sinh(phi_r))*(-cosh(2.0d0*phi_r) + 3.0d0*c1*sinh(2.0d0*phi_r))/phi_0
!V(3) = 2.0d0*V0*(-9.0d0*c1*c1*cosh(2.0d0*phi_r) + (1.0d0 + 9.0d0*c1*c1)*cosh(4.0d0*phi_r) &
!	& + 3.0d0*c1*(sinh(2.0d0*phi_r) - 2.0d0*sinh(4.0d0*phi_r)))/(phi_0*phi_0)
!----------------------------------------------------------------------------------------------------------------------------------------------

if (include_step) then

coupler = 1.55177d-3
phi_step = 14.6616d0						! ref. mathematica file ne-ng-qp-ws-july-15-2011.nb
phi_width = 2.60584d-2

!coupler = 1.552d-3
!phi_step = 14.66d0						! against fortran code potential.f90
!phi_width = 260.6d-4

phi_r = (phi-phi_step)/phi_width

stepper(1) = (1+coupler*tanh(phi_r))
stepper(2) = (coupler/(phi_width*(cosh(phi_r))**2))
stepper(3) = (-2*coupler*sinh(phi_r)/((phi_width**2)*(cosh(phi_r))**3))

V(1) = V(1)*stepper(1)								
V(2) = V(2)*stepper(1)+V(1)*stepper(2)
V(3) = V(3)*stepper(1) + 2*V(2)*stepper(2) + V(1)*stepper(3)	
		
end if

!----------------------------------------------------------------------------------------------------------------------------------------------

if (include_bump) then

!coupler = 1.8737d-3
!phi_bump = 2.005d0					! KKLT - Swagat et.al. 1911.00057 Table 1 row 1 - except for coupler
!phi_width = 1.993d-2

coupler = 1.1694d-3
phi_bump = 2.188d0					! KKLT - Swagat et.al. 1911.00057 Table 3 row 1 - except for coupler
phi_width = 1.590d-2

!coupler = 2.5d-3
!phi_bump = 4.20d0						! for Staro
!phi_width = 3.0d-2

!coupler = 4.305d-3
!phi_bump = 9.7d0						! for small field
!phi_width = 3.0d-2

phi_r = (phi-phi_bump)/phi_width

bumper(1) = 1.0d0 + coupler*dexp(-phi_r*phi_r/2.0d0)
bumper(2) = -coupler*phi_r*dexp(-phi_r*phi_r/2.0d0)/phi_width
bumper(3) = coupler*dexp(-phi_r*phi_r/2.0d0)*(phi_r*phi_r - 1.0d0)/(phi_width*phi_width)

V(1) = V(1)*bumper(1)								
V(2) = V(2)*bumper(1) + V(1)*bumper(2)						
V(3) = V(3)*bumper(1) + 2.0d0*V(2)*bumper(2) + V(1)*bumper(3)	
		
end if

!----------------------------------------------------------------------------------------------------------------------------------------------

if (include_dip) then

coupler = 2.207d-3
phi_dip = 2.175d0					! KKLT - Swagat et.al. 1911.00057 Table 3 row 1 - except for coupler
phi_width = 2.742d-2

!coupler = 2.58d-3
!phi_dip = 4.25d0						! for Staro
!phi_width = 2.8d-2

!coupler = 2.0d-3
!phi_dip = 12.0d0						! for small field
!phi_width = 2.8d-2

phi_r = (phi-phi_dip)/phi_width

dipper(1) = 1.0d0 - coupler*dexp(-phi_r*phi_r/2.0d0)
dipper(2) = coupler*phi_r*dexp(-phi_r*phi_r/2.0d0)/phi_width
dipper(3) = -coupler*dexp(-phi_r*phi_r/2.0d0)*(phi_r*phi_r - 1.0d0)/(phi_width*phi_width)

V(1) = V(1)*dipper(1)								
V(2) = V(2)*dipper(1) + V(1)*dipper(2)						
V(3) = V(3)*dipper(1) + 2.0d0*V(2)*dipper(2) + V(1)*dipper(3)	
		
end if

!----------------------------------------------------------------------------------------------------------------------------------------------

End function V


 FUNCTION Hfunc(g,phi,phiN,phiNN)
Double Precision :: phi, phiN, phiNN, l
Double Precision, dimension(3) :: g
Double Precision, dimension(2) :: Hfunc

!l = sqrt(abs(3.0d0-phiN*phiN/2.0d0))
!if (l .le. 1.0d-16) then
!l = 1.0d-16
!end if

!Hfunc(1) = sqrt(g(1)/abs(3.0d0-phiN*phiN/2.0d0))
!Hfunc(1) = sqrt(g(1))/l
!Hfunc(2) = 0.5d0*sqrt(1/g(1))*(g(2)*phiN/l + g(1)*phiNN*phiN/(l**3))				! derivative w.r.t e-fold

l = 3.0d0-phiN*phiN/2.0d0
Hfunc(1) = sqrt(g(1)/(3.0d0 - phiN*phiN/2.0d0))			! testing
Hfunc(2) = 0.5d0*(g(2)*phiN/sqrt(l*g(1)) + g(1)*phiNN*phiN/sqrt(g(1)*(3.0d0-(phiN*phiN)/2.0d0)**3))	! testing

End function Hfunc

 Function zfunc(e,phiN,phiNN,phiNNN,H)
Double precision :: e, phiN, phiNN, phiNNN
Double precision, dimension(2) :: H
Double precision, dimension(3) :: zfunc

zfunc(1) = -ai*exp(e)*phiN					! since sqrt(2*eps1) = - d\phi/dN				
zfunc(2) = zfunc(1) - ai*exp(e)*phiNN				! derivative w.r.t. e-fold
zfunc(3) = ((ai*dexp(e)*H(1))*(ai*dexp(e)*H(1)))*((zfunc(2)/zfunc(1))*(3.0 + H(2)/H(1)) &
		& + phiNNN/phiN - 1.0d0)			! z"/z

End function zfunc

!----------------------------------------------------------------------------------------------------------------------------------------------
! background fns.
!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 FUNCTION f1(i, e, a, b, fi, pe)
	
	integer :: i
	double precision :: e
	complex*16 :: a, b
	Type(BG) :: fi
	Type(Pert) :: pe

		f1 = b

End function f1

Complex*16 FUNCTION f2(i, e, a, b, fi, pe)

	integer :: i
	double precision :: e, philoc, phiNloc, phiNNloc
	double precision, dimension(3) :: Vloc
	double precision, dimension(2) :: Hloc
	complex*16 :: a, b
	Type(BG) :: fi
	Type(Pert) :: pe
	
	philoc = DBLE(a)
	phiNloc = DBLE(b)
	Vloc(:) = V(philoc,fi%paramets)

!	if(abs(Vloc(1)) .lt. 1.0d-16) then
!		if (Vloc(1) .le. 0.0d0) then
!		Vloc(1) = -1.0d-16
!		else if (Vloc(1) .gt. 0.0d0) then
!		Vloc(1) = 1.0d-16
!		end if
!	end if

!	if(abs(philoc) .le. 1.0d-7) then
!	print*, e, philoc, Vloc(1) 
!		if(philoc .le. 0.0d0) then
!		philoc = -1.0d-7
!		Vloc(:) = V(philoc,fi%paramets)			! testing
!		else if(philoc .gt. 0.0d0) then
!		philoc = 1.0d-7
!		Vloc(:) = V(philoc,fi%paramets)			! testing
!		end if
!	end if

	phiNNloc = -((3.0d0-phiNloc*phiNloc/2.0d0)*phiNloc + Vloc(2)*(3.0d0-phiNloc*phiNloc/2.0d0)/Vloc(1))
	Hloc(:)	= Hfunc(Vloc,philoc,phiNloc,phiNNloc)

	f2 = -((3.0d0-b*b/2.0d0)*b + Vloc(2)/(Hloc(1)*Hloc(1)))

End function f2

Subroutine backqties(i, e, s, a, b, fi)

	Integer :: i
	Double precision :: e, s
	Complex*16 :: a, b
	Type(BG) :: fi

	fi%efold(i) = e
	fi%estep(i) = s
	fi%phi(i) = DBLE(a)
	fi%phiN(i) = DBLE(b)

	fi%Pot(:,i) = V(fi%phi(i),fi%paramets)
	fi%phiNN(i) = -((3-fi%phiN(i)**2/2)*fi%phiN(i) + fi%Pot(2,i)*(3-fi%phiN(i)**2/2)/(fi%Pot(1,i)))
	fi%H(:,i) = Hfunc(fi%Pot(:,i),fi%phi(i),fi%phiN(i),fi%phiNN(i))
	fi%phiNNN(i) = -(3.0*fi%phiNN(i)*(1.0d0-fi%phiN(i)**2/2) + fi%Pot(2,i)*(fi%phiN(i)/fi%H(1,i))**2 &
			& + fi%Pot(3,i)*fi%phiN(i)/fi%H(1,i)**2)
	fi%NOOOS = i

End subroutine backqties

!----------------------------------------------------------------------------------------------------------------------------------------------
! perturbations fns.
!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 FUNCTION f3(i, e, a, b, fi, pe)

	integer :: i
	double precision :: e
	complex*16 :: a, b
	Type(BG) :: fi
	Type(Pert) :: pe

		f3 = b

End function f3

Complex*16 FUNCTION f4(i, e, a, b, fi, pe)
	
	integer :: i
	double precision :: e, s, philoc, phiNloc, phiNNloc, phiNNNloc
	double precision, dimension(3) :: Vloc, zloc
	double precision, dimension(2) :: Hloc
	complex*16 :: a, b
	Type(BG) :: fi
	Type(Pert) :: pe

if (abs(e-fi%efold(i)) .ge. 1.0d0/DBLE(NOS)) then

	philoc = fi%phi(i)
	phiNloc = fi%phiN(i)

	Vloc(:) = V(philoc,fi%paramets)
	Hloc(:) = Hfunc(Vloc,philoc,phiNloc,phiNNloc)
	
	phiNNloc = -((3-phiNloc*phiNloc/2)*phiNloc + Vloc(2)*(3-phiNloc*phiNloc/2)/Vloc(1))
	phiNNNloc = -(3.0*phiNNloc*(1-phiNloc*phiNloc/2) + Vloc(2)*(phiNloc/Hloc(1))*(phiNloc/Hloc(1)) &
			& + Vloc(3)*phiNloc/(Hloc(1)*Hloc(1)))
	zloc(:) = zfunc(e,phiNloc,phiNNloc,phiNNNloc,Hloc)

	f4 = -(1.0d0 + (Hloc(2)/Hloc(1)) + 2*zloc(2)/zloc(1))*b - ((pe%k/(ai*dexp(e)*Hloc(1)))*(pe%k/(ai*dexp(e)*Hloc(1))))*a
else
	f4 = -(1.0d0 + (fi%H(2,i)/fi%H(1,i)) + 2*fi%z(2,i)/fi%z(1,i))*b - ((pe%k/(ai*dexp(e)*fi%H(1,i)))**2)*a
end if

End function f4
	
!----------------------------------------------------------------------------------------------------------------------------------------------
	
Complex*16 FUNCTION f5(i, e, a, b, fi, pe)

	integer :: i
	double precision :: e
	complex*16 :: a, b
	Type(BG) :: fi
	Type(Pert) :: pe
		f5 = b

End function f5

Complex*16 FUNCTION f6(i, e, a, b, fi, pe)

	integer :: i
	double precision :: e, s, philoc, phiNloc, phiNNloc
	double precision, dimension(3) :: Vloc
	double precision, dimension(2) :: Hloc
	complex*16 :: a, b
	Type(BG) :: fi
	Type(Pert) :: pe

if (abs(e-fi%efold(i)) .ge. 1.0d0/DBLE(NOS)) then

	philoc = fi%phi(i)
	phiNloc = fi%phiN(i)

	Vloc(:) = V(philoc,fi%paramets)
	Hloc(:) = Hfunc(Vloc,philoc,phiNloc,phiNNloc)
	
	f6 = -(3.0d0 + Hloc(2)/Hloc(1))*b - ((pe%k/(ai*dexp(e)*Hloc(1)))*(pe%k/(ai*dexp(e)*Hloc(1))))*a
else
	f6 = -(3.0d0 + fi%H(2,i)/fi%H(1,i))*b - ((pe%k/(ai*dexp(e)*fi%H(1,i)))**2)*a
end if

End function f6

Subroutine pertqties(i, e, s, a, b, fi, pe)

	Integer :: i
	Double precision :: e, s
	Complex*16 :: a, b
	Type(BG) :: fi
	Type(Pert) :: pe

	return

end subroutine pertqties

!----------------------------------------------------------------------------------------------------------------------------------------------
! 3 pt fns.
!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR1(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(1) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: k1,k2,k3,e,s,eps1,t4,t7,cutoff,kappa
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0				! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t1 = ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i)*t4*t4
t2 = ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i)*t4*t4				! Note: dN = a*H*deta
t3 = ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i)*t4*t4

t6 = 2.0d0*II*(Field1%a(i)/Field1%H(1,i))*eps1*eps1*(t1+t2+t3)*t5*cutoff
t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR1 = t6

End function GRRR1

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR2(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(2) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: e,s,eps1,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t6 = -2.0d0*II*(k1k2+k2k3+k1k3)*(Field1%a(i)/Field1%H(1,i)) &
	& *eps1*eps1*ACurv1%RC(i)*ACurv2%RC(i)*ACurv3%RC(i)*t5*cutoff
t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR2 = t6

End function GRRR2

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR3(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(3) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: e,s,eps1,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t1 = (k1k2/k2k2 + k1k3/k3k3)*(ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i))*t4*t4
t2 = (k1k2/k1k1 + k2k3/k3k3)*(ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i))*t4*t4
t3 = (k1k3/k1k1 + k2k3/k2k2)*(ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i))*t4*t4			! Note: dN = a*H*deta

t6 = -2.0d0*II*(Field1%a(i)/Field1%H(1,i))*eps1*eps1*(t1+t2+t3)*t5*cutoff
t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR3 = t6

End function GRRR3

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR13(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(1)+\G_RRR(3) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: e,s,eps1,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t1 = ( ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i) + (k1k2/k2k2 + k1k3/k3k3)*(ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i)) )*t4*t4
t2 = ( ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i) + (k1k2/k1k1 + k2k3/k3k3)*(ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i)) )*t4*t4
t3 = ( ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i) + (k1k3/k1k1 + k2k3/k2k2)*(ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i)) )*t4*t4			! Note: dN = a*H*deta

t6 = -2.0d0*II*(Field1%a(i)/Field1%H(1,i))*eps1*eps1*(t1+t2+t3)*t5*cutoff

t6 = t6 - 2.0d0*II*(Field1%a(i)/Field1%H(1,i))*eps1*eps1*(k1k2+k2k3+k1k3) &
	& *(ACurv1%RC(i)*ACurv2%RC(i)*ACurv3%RC(i))*t5*cutoff

t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR13 = t6

End function GRRR13

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR123(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(123) term  Ref.:arxiv.org/pdf/1211.6753

Integer :: i,ievol1,ievol2,ievol3
Double precision :: e,s,eps1,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t2 = (1.0/k1k1 + 1.0/k2k2 + 1.0/k3k3)*(ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RCN(i))*t4*t4*t4

t6 = -8*II*(Field1%a(i)*Field1%a(i))*eps1*eps1*t2*t5*cutoff
t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR123 = t6

End function GRRR123

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR4(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(4) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: k1,k2,k3,e,s,eps1,eps2N,eps2Neps1,t4,t7,cutoff,kappa
Double precision :: eps2Neps11, eps2Neps12
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

eps2Neps11 = Field1%phiNNN(i)*Field1%phiN(i)
eps2Neps12 = Field1%phiNN(i)*Field1%phiNN(i)

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
!t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))
!	cutoff = 1.0d0

eps2Neps1 = (eps2Neps11 - eps2Neps12)

t1 = ACurv1%RC(i)*ACurv2%RC(i)*ACurv3%RCN(i)*t4
t2 = ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RC(i)*t4					! Note: dN = a*H*deta
t3 = ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RC(i)*t4

t6 = II*(Field1%a(i)*Field1%a(i))*eps2Neps1*(t1+t2+t3)*t5*cutoff
t6 = t6 + DCONJG(t6)

GRRR4 = t6

End function GRRR4

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR24(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(4) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: e,s,eps1,eps2N,eps2Neps1,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t6 = -2.0d0*II*(k1k2+k2k3+k1k3)*(Field1%a(i)/Field1%H(1,i)) &
	& *eps1*eps1*ACurv1%RC(i)*ACurv2%RC(i)*ACurv3%RC(i)*t5*cutoff
t6 = t6 + DCONJG(t6)

	cutoff = 1.0d0

eps2N = (Field1%phiNNN(i)*Field1%phiN(i) - Field1%phiNN(i)**2)/eps1		! d(eps_2)/dN = (phi_NNN*phi_N - phiNN^2)/eps1
eps2Neps1 = (Field1%phiNNN(i)*Field1%phiN(i) - Field1%phiNN(i)**2)

t1 = ACurv1%RC(i)*ACurv2%RC(i)*ACurv3%RCN(i)*t4
t2 = ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RC(i)*t4							! Note: dN = a*H*deta
t3 = ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RC(i)*t4

t6 = t6 + II*(Field1%a(i)/Field1%H(1,i))*eps2Neps1*(t1+t2+t3)*t5*cutoff
t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR24 = t6

End function GRRR24

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR5(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(4) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: e,s,eps1,eps2N,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t1 = (k1k2/k2k2 + k1k3/k3k3)*(ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i))*t4*t4
t2 = (k1k2/k1k1 + k2k3/k3k3)*(ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i))*t4*t4			! Note: dN = a*H*deta
t3 = (k1k3/k1k1 + k2k3/k2k2)*(ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i))*t4*t4

t6 = (II/2.0d0)*(Field1%a(i)/Field1%H(1,i))*eps1*eps1*eps1*(t1+t2+t3)*t5*cutoff
t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR5 = t6

End function GRRR5

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR6(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(4) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: e,s,eps1,eps2N,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t1 = (k1k1*k2k3/(k2k2*k3k3))*(ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i))*t4*t4
t2 = (k2k2*k1k3/(k1k1*k3k3))*(ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i))*t4*t4			! Note: dN = a*H*deta
t3 = (k3k3*k1k2/(k1k1*k2k2))*(ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i))*t4*t4

t6 = (II/2.0d0)*(Field1%a(i)/Field1%H(1,i))*eps1*eps1*eps1*(t1+t2+t3)*t5*cutoff
t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR6 = t6

End function GRRR6

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR56(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(4) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: e,s,eps1,eps2N,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

		e = Field1%efold(i)
		s = Field1%estep(i+1)						! step size corr. to index ?check?

		eps1 = (Field1%phiN(i)**2)/2.0d0						! eps_1 = phiN^2/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t4))
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t1 = ( (k1k2/k2k2 + k1k3/k3k3)*(ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i)) + &
	& (k1k1*k2k3/(k2k2*k3k3))*(ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i)) )*t4*t4
t2 = ( (k1k2/k1k1 + k2k3/k3k3)*(ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i)) + &
	& (k2k2*k1k3/(k1k1*k3k3))*(ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i)) )*t4*t4			! Note: dN = a*H*deta
t3 = ( (k1k3/k1k1 + k2k3/k2k2)*(ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i)) + &
	& (k3k3*k1k2/(k1k1*k2k2))*(ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i)) )*t4*t4

t6 = (II/2.0d0)*(Field1%a(i)/Field1%H(1,i))*eps1*eps1*eps1*(t1+t2+t3)*t5*cutoff
t6 = t6 + DCONJG(t6)
!t6 = 2.0d0*DBLE(t6)

GRRR56 = t6

End function GRRR56

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRR7(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(2) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: k1,k2,k3,eps1,eps2,t4,t7,cutoff,kappa
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

eps2 = 2.0d0*Field1%phiNN(i)/Field1%phiN(i)						! eps_2 = 2*phiNN/phiN
eps1 = Field1%phiN(i)*Field1%phiN(i)/2.0d0						! eps_1 = phiN*phiN/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

t1 = ACurv1%RC(i)*ACurv2%RC(i)*ACurv3%RCN(i)*t4*t5
t2 = ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RC(i)*t4*t5
t3 = ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RC(i)*t4*t5

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*DBLE(t4)))							! testing
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t6 = -II*eps1*eps2*(t1 + t2 + t3)*Field1%a(i)*Field1%a(i)*cutoff

GRRR7 = t6 + DCONJG(t6)

End function GRRR7

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRRB1(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(2) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: eps1,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

eps1 = Field1%phiN(i)*Field1%phiN(i)/2.0d0					! eps_1 = phiN/phiN/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

t1 = ACurv1%RC(i)*ACurv2%RC(i)*ACurv3%RC(i)*t5
t2 = (54.0d0*t4*t4 + 2.0d0*(1-eps1)*(k1k2 + k2k3 + k1k3) + (k1k2*k3k3 + k2k3*k1k1 + k1k3*k2k2)/(2.0d0*t4*t4))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*DBLE(t4)))							! testing
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t6 = -II*(Field1%a(i)/Field1%H(1,i))*t1*t2*cutoff

GRRRB1 = t6 + DCONJG(t6)

End function GRRRB1

!----------------------------------------------------------------------------------------------------------------------------------------------

Complex*16 Function GRRRB2(i,Field1,ACurv1,ACurv2,ACurv3,kappa)			! integrand of \G_RRR(2) term

Integer :: i,ievol1,ievol2,ievol3
Double precision :: eps1,t4,t7,cutoff,kappa
Double precision :: k1,k2,k3,k1k1,k2k2,k3k3,k1k2,k2k3,k1k3
Complex*16 :: t1,t2,t3,t5,t6

Type(BG) :: Field1
Type(Pert)::ACurv1,ACurv2,ACurv3

eps1 = Field1%phiN(i)*Field1%phiN(i)/2.0d0					! eps_1 = phiN/phiN/2

ievol1 = ACurv1%i_z
ievol2 = ACurv2%i_z
ievol3 = ACurv3%i_z

k1 = ACurv1%k
k2 = ACurv2%k
k3 = ACurv3%k

k1k1 = ACurv1%k*ACurv1%k
k2k2 = ACurv2%k*ACurv2%k
k3k3 = ACurv3%k*ACurv3%k

k1k2 = (k3k3-k1k1-k2k2)/(2.0d0)
k2k3 = (k1k1-k2k2-k3k3)/(2.0d0)
k1k3 = (k2k2-k1k1-k3k3)/(2.0d0)

t1 = 0
t2 = 0
t3 = 0
t4 = Field1%a(i)*Field1%H(1,i)
t5 = ACurv1%R(ievol1)*ACurv2%R(ievol2)*ACurv3%R(ievol3)
t6 = 0
t7 = sqrt(abs(Field1%z(3,i)))

	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*DBLE(t4)))							! testing
!	cutoff = dexp(-kappa*(k1+k2+k3)/(3.0d0*t7))

t1 = (k1k1 - k1k3*k1k3/k3k3 + k2k2 - k2k3*k2k3/k3k3)*ACurv1%RC(i)*ACurv2%RC(i)*ACurv3%RCN(i)*t4
t2 = (k1k1 - k1k2*k1k2/k2k2 + k3k3 - k2k3*k2k3/k2k2)*ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RC(i)*t4
t3 = (k2k2 - k1k2*k1k2/k1k1 + k3k3 - k1k3*k1k3/k1k1)*ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RC(i)*t4

t6 = II*(eps1/(2.0d0*Field1%H(1,i)*Field1%H(1,i)))*(t1 + t2 + t3)*t5*cutoff

t1 = (2.0d0 - eps1*(1.0d0 - (k2k3*k2k3)/(k2k2*k3k3)))*ACurv1%RC(i)*ACurv2%RCN(i)*ACurv3%RCN(i)*t4*t4
t2 = (2.0d0 - eps1*(1.0d0 - (k1k3*k1k3)/(k1k1*k3k3)))*ACurv1%RCN(i)*ACurv2%RC(i)*ACurv3%RCN(i)*t4*t4
t3 = (2.0d0 - eps1*(1.0d0 - (k1k2*k1k2)/(k2k2*k1k1)))*ACurv1%RCN(i)*ACurv2%RCN(i)*ACurv3%RC(i)*t4*t4

t6 = t6 - II*eps1*(Field1%a(i)/Field1%H(1,i))*(t1 + t2 + t3)*t5*cutoff

GRRRB2 = t6 + DCONJG(t6)

End function GRRRB2

!----------------------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------------------------------------
End Module HV
!----------------------------------------------------------------------------------------------------------------------------------------------
