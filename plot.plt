
set term pdf; set output "back_evol.pdf";

set xlabel "N"; set ylabel "phi";
plot "back_evol.txt" u 2:3 w l lw 2 title "phi(N)"
set xlabel "N"; set ylabel "log_{10} epsilon_1";
plot "back_evol.txt" u 2:(log10($4)) w l lw 2 title "eps1(N)"
set xlabel "phi"; set ylabel "V(phi)";
plot "back_evol.txt" u 3:5 w l lw 2 title "V(\phi)"
set xlabel "N"; set ylabel "H(N)";
plot "back_evol.txt" u 2:6 w l lw 2 title "H(N)"

set term pdf; set output "PPS.pdf";

set xlabel "k/Mpc^{-1}"; set ylabel "P_{s,t}(k)";
set format "%g"; set log xy;
plot "PPS_output.txt" u (10**$1):2 w l lw 2 title "scalars", "" u (10**$1):3 w l lw 2 title "tensors"

set term pdf; set output "PBS.pdf";

set xlabel "k/Mpc^{-1}"; set ylabel "G(k)";
plot "PBS_output.txt" u (($1)):(abs($2+$4)) w l lw 2 title "G1+G3", \
 "PBS_output.txt" u (abs($1)):(abs($3)) w l lw 2 title "G2", \
 "PBS_output.txt" u (abs($1)):(abs($5+$8-$9)) w l lw 2 title "G4+G7", \
 "PBS_output.txt" u (abs($1)):(abs($6+$7)) w l lw 2 title "G5+G6", \
 "PBS_output.txt" u (abs($1)):(abs($10-$11)) w l lw 2 title "G8", \
 "PBS_output.txt" u (abs($1)):(abs($12-$13)) w l lw 2 title "G9"

set xlabel "k/Mpc^{-1}"; set ylabel "f_{NL}(k)";
set format "%g"; unset log; set log x;
plot "PBS_output.txt" u (($1)):($15) w l lw 2 title ""

set term qt; replot
