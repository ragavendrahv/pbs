Module Params

IMPLICIT NONE

Integer, parameter :: n = 3, Ni = 0, Nf = 120
Integer, parameter :: no_cores = 8
Complex*16, parameter :: II = DCMPLX(0,1)
Double precision, parameter :: PI = 4.0d0*atan(1.0d0)

!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 12.0d0, e_i = 2.0d-3, phiN_i = -sqrt(2*e_i)			!RIPI
!Double precision, parameter :: m = 7.167d-8, phi_0 = 1.9654d0, Np = 90.0d0				!RIPI
!----------------------------------------------------------------------------------------------------------------------------------------------
Double precision, parameter :: phi_i = 16.0d0, e_i = 1.0d-2, phiN_i = -sqrt(2*e_i) 		!m2phi2-std.
Double precision, parameter :: m = 7.0d-6, phi_0 = 1.0d0, Np = 50.0d0					!m2phi2-std.

!Double precision, parameter :: phi_i = 15.48d0, e_i = 8.3d-3, phiN_i = -sqrt(2*e_i) 	!m2phi2-HCO-trial
!Double precision, parameter :: m = (6.48779d-6), phi_0 = 12.0d0						!m2phi2-HCO-trial

!Double precision, parameter :: phi_i = 18.85d0, e_i = 2.99d0, phiN_i = -sqrt(2*e_i)		!m2phi2-KDI-Hergt
!Double precision, parameter :: m = 5.0d-6, phi_0 = 1.0d0, Np = 56.838240d0					!m2phi2-KDI-Hergt

!Double precision, parameter :: phi_i = 18.85d0, e_i = 2.99d0, phiN_i = -sqrt(2.0*e_i)	!m2phi2-KDI-2pt99
!Double precision, parameter :: m = 6.16595002d-6, phi_0 = 1.0d0, Np = 56.838240		!m2phi2-KDI-2pt99

!Double precision, parameter :: phi_i = 18.85d0, e_i = 2.99d0, phiN_i = -sqrt(2.0*e_i)	!m2phi2-KDI-1pt00
!Double precision, parameter :: m = -4.0537120d-1, phi_0 = 1.0d0, Np = 56.71505d0	!m2phi2-KDI-1pt00

!Double precision, parameter :: phi_i = 16.0d0, e_i = 1.0d-4, phiN_i = -sqrt(2.0*e_i)	!m2phi2-KDI-dual
!Double precision, parameter :: m = -4.0632840d-1, phi_0 = 1.0d0, Np = 56.87435d0	!m2phi2-KDI-dual
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 5.6d0, e_i = 1.0d-3, phiN_i = -sqrt(2*e_i)	!Staro
!Double precision, parameter :: m = (1.2d-9)**(0.25), phi_0 = sqrt(1.5d0)			!Staro

!Double precision, parameter :: phi_i = 5.6d0, e_i = 1.0d-4, phiN_i = -sqrt(2*e_i)			!Staro - for dipper PBH
!Double precision, parameter :: m = (1.2d-9)**(0.25), phi_0 = sqrt(1.5d0), Np = 58.0d0		!Staro - for dipper PBH

!Double precision, parameter :: phi_i = 5.6d0, e_i = 1.0d-4, phiN_i = -sqrt(2*e_i)			!Staro - for bumper PBH
!Double precision, parameter :: m = (1.2d-9)**(0.25), phi_0 = sqrt(1.5d0), Np = 50.0d0		!Staro - for bumper PBH

!Double precision, parameter :: phi_i = 8.3752d0, e_i = 2.99d0, phiN_i = -sqrt(2*e_i)		!Staro-KDI-2pt99
!Double precision, parameter :: m = 9.69304d-1, phi_0 = sqrt(1.5d0), Np = 54.56345d0		!Staro-KDI-2pt99

!Double precision, parameter :: phi_i = 8.3752d0, e_i = 2.99d0, phiN_i = -sqrt(2*e_i)		!Staro-KDI-1pt00
!Double precision, parameter :: m = 9.496466d-1, phi_0 = sqrt(1.5d0), Np = 55.69306d0		!Staro-KDI-1pt00

!Double precision, parameter :: phi_i = 5.52d0, e_i = 1.0d-4, phiN_i = -sqrt(2*e_i)			!Staro-KDI-dual
!Double precision, parameter :: m = 9.8048240d-1, phi_0 = sqrt(1.5d0), Np = 53.37404d0		!Staro-KDI-dual
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 7.5d0, e_i = 1.0d-3, phiN_i = sqrt(2*e_i)	!small field
!Double precision, parameter :: m = (5.5d-10)**(0.25), phi_0 = 15.0d0				!small field

!Double precision, parameter :: phi_i = 7.5d0, e_i = 1.0d-3, phiN_i = sqrt(2*e_i)		!small field - for bump
!Double precision, parameter :: m = (5.5d-10)**(0.25), phi_0 = 15.0d0, Np = 50.0d0		!small field - for bump

!Double precision, parameter :: phi_i = 4.95d0, e_i = 2.99d0, phiN_i = sqrt(2*e_i)	!small field - KDI
!Double precision, parameter :: m = (3.6d-10)**(0.25), phi_0 = 15.0d0				!small field - KDI
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 13.5d0, e_i = 2.99d0, phiN_i = -sqrt(2*e_i)			!AxM
!Double Precision, parameter ::  m = (1.6d-10)**(1.0d0/3.0d0), phi_0 = 7.6346d-3, kappa = 0.05d0	!AxM - DC
!Double Precision, parameter ::  m = (2.513d-10)**(1.0d0/3.0d0), phi_0 = 4.50299d-4, kappa = 0.05d0	!AxM - BINGO
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = -10.0d0, e_i = 1.0d-2, phiN_i = -sqrt(2*e_i)			!SFPI
!Double Precision, parameter ::  m = 0.4d-11, phi_0 = 4.0d0, Np = 50.0d0				!SFPI
!----------------------------------------------------------------------------------------------------------------------------------------------
!Double precision, parameter :: phi_i = 0.849d0, e_i = 1.0d-4, phiN_i = -sqrt(2*e_i)	!Staro-II
!Double precision, parameter :: m = (2.37d-12)**(0.25), phi_0 = 0.707, kappa = 0.3d0	!Staro-II, Ref.: Martin, Sriram

!Double precision, parameter :: phi_i = 8.4348d0, e_i = 1.0d-4, phiN_i = -sqrt(2*e_i)	!Staro-II
!Double precision, parameter :: m = log10(2.98d1), phi_0 = 5.628d0, Np = 44.50d0		!Staro-II, against KDI
!----------------------------------------------------------------------------------------------------------------------------------------------

!Double precision, parameter :: Np = 50.0d0
Double precision :: ai
!Double precision, parameter :: ai = 7.3570280d-3
Logical, parameter :: aiset = .FALSE.

Integer, parameter :: Nic_func = 1
!Nic_func = 1 => aH ; Nic_func = 2 => z"/z

Double precision, parameter :: Nic_depth = 1.0d2
Double precision, parameter :: kp = 5.0d-2, kmin = 1.0d-6, kmax = 5.0d0, k_test = 5.0d-2
Double precision, parameter :: k_kappa = 1.0d-1, ksqueez = 1.0d-7, k2D = 5.0d-2, kappa = 0.3d0

Integer, parameter :: NOS = 7000, Nadap = 1000, Nef = Nf-Ni				! NOS minimum 10^4 required for PPS;
Integer, parameter :: NOSD = 100, NOS2D = NOSD!*NOSD	 				! NOS2D - pts. in 2D plane of k2/k1-k3/k1
Double precision, parameter :: toler = 1.0d-7						! testing for RIPI

Double precision :: paramet1 = m, paramet2 = phi_0				! usually those to be constrained

Logical, parameter :: atype = .TRUE., btype = .FALSE.					! to choose b/w eps1i = 2.99 or 1.00

Logical, parameter :: include_step = .FALSE., include_bump = .FALSE., include_dip = .FALSE.

Logical, parameter ::  do_perturb = .TRUE., do_mode_test = .FALSE., do_RR = .TRUE., do_RRR = .TRUE., &
			& do_bulk = .TRUE., do_boundaries = .FALSE., &
			& do_kappa = .FALSE., do_equil = .TRUE., do_squeez = .FALSE., &
			& do_2D = .FALSE., do_3D = .FALSE.
!----------------------------------------------------------------------------------------------------------------------------------------------
Type BG
Integer :: NOOOS
Double Precision, dimension(:), allocatable :: efold, estep
Double Precision, dimension(:), allocatable :: phi, phiN
Double Precision, dimension(:), allocatable :: phiNN, phiNNN, a, zNN
Double Precision, dimension(:,:), allocatable :: Pot
Double Precision, dimension(:,:), allocatable :: H, z
Double Precision, dimension(2) :: paramets
End Type BG
!----------------------------------------------------------------------------------------------------------------------------------------------
Type Pert
Integer :: i_a, i_z, i_s
Double Precision :: ic_con, shs_con, star_con, Nic, Nshs, Nstar
Double Precision :: k, SPS, TPS, Rerr, Terr, SSRA
Complex*16, dimension(:), allocatable :: R, RN, Rgr, Rde
Complex*16, dimension(:), allocatable :: RC, RCN
Complex*16, dimension (:), allocatable :: T, TN
Complex*16, dimension(:), allocatable :: TC, TCN
Complex*16, dimension(:), allocatable :: S
End Type Pert
!----------------------------------------------------------------------------------------------------------------------------------------------
End Module Params
!----------------------------------------------------------------------------------------------------------------------------------------------
!----------------------------------------------------------------------------------------------------------------------------------------------
!for m2phi2
! N = 61, m=5.0d-6, phi_0=irrelevant, phi_i = 15.5, e_i = 1.0d-2
!----------------------------------------------------------------------------------------------------------------------------------------------
! for m2phi2_KDI compared against contaldi
! phi_i = 17.65d0, e_i = 2.99d0, m = 7.0d-6, kmin=1e-6, kmax=1.0d1, kp=0.05, toler=5.0d-14
!----------------------------------------------------------------------------------------------------------------------------------------------
!for m2phi2_hardcut_NIC compared against contaldi
! phi_i = 14.72d0, e_i =1.0d-2, phiN_i = -sqrt(2*e_i), toler=1.0d-12
!----------------------------------------------------------------------------------------------------------------------------------------------
!for m2phi2 hard cutoff on attractor
! phi_i = 17.65d0, e_i = 65.0d-4, m = 7.18d-6, Np=50, Nf=60, toler=6.0d-15
! Nboi : 2eps1=eps2=eps3
!----------------------------------------------------------------------------------------------------------------------------------------------
!for m2phi2_KDI - cutoff for G4
! phi_i = 18.7d0, e_i = 2.99d0, phiN_i = -sqrt(2*e_i), m = 5.0d-6, Np = 57.48d0
!----------------------------------------------------------------------------------------------------------------------------------------------
!for m2phi2_KDI_Hergt best fit values
! phi_i = 18.841d0, e_i = 2.99d0, phiN_i = -sqrt(2*e_i), m = 5.0d-6, Np = 57.48d0
!----------------------------------------------------------------------------------------------------------------------------------------------
!for m2phi2_step
! phi_i = 16.5d0, e_i = 1.0d-4, m = 7.18d-6, phi_0 = 1.0d0
!----------------------------------------------------------------------------------------------------------------------------------------------
! for staro
! Nf=70, m = (1.0d-10)**(0.237), phi_0=sqrt(3/2) with phi_i = 5.6 (in gen. greater than 1) and e_i = 1.0d-3 (close to 0)
!----------------------------------------------------------------------------------------------------------------------------------------------
! for small field model
! Nf=68, m=4.57x10^-(2.5), phi_star = 15.0d0, phi_i=7.5, e_i=0.0, phi_0 = 7.95d0, phi_c=0.75d-2, c=-1.5d-4	
! V0 = m**4 = 4.586d-10
! modified
!Double precision, parameter :: phi_i = 7.5d0, e_i = 1.0d-3			
!Double precision, parameter :: m = (3.2d-10)**(0.25), phi_0 = 15.0d0
!----------------------------------------------------------------------------------------------------------------------------------------------
! for small field model modified to compare Hergt
! Np = 50.4d0
!Double precision, parameter :: phi_i = 4.95d0, e_i = 2.99d0, phiN_i = sqrt(2*e_i)
!Double precision, parameter :: m = (3.6d-10)**(0.25) = 4.355d-3 , phi_0 = 15.0d0				
!----------------------------------------------------------------------------------------------------------------------------------------------
!for amm
! Double Precision, parameter :: phi_star = 7.6346d-3, m = (2.512d-10)**(1./3.), phi_i = 11.5, e_i = 1.0d-4

!modified to compare Hergt
! Np = 51.6d0
!Double precision, parameter :: phi_i = 13.5d0, e_i = 2.99d0, phiN_i = -sqrt(2*e_i)	!AxM
!Double Precision, parameter ::  m = (1.6d-10)**(1.0d0/3.0d0), phi_0 = 7.6346d-3	!AxM - DC
!----------------------------------------------------------------------------------------------------------------------------------------------
!for punc. inf. (RIPI n=3)
!param1=phi_0 = 1.95964d0, param2=log10(m) = log10(1.5012E-7) phi_i =11.5 Nf= 82
! ai = 1.062d-2
! cutoff for G4, Nic:k=1000aH, kappa=0.02
! RIPI_new phi_i = 21.0d0
!----------------------------------------------------------------------------------------------------------------------------------------------
! for Staro-II, Ref.: Martin, Sriram
! m = (2.37d-12)**(0.25), phi_0 = 0.707, phi_i = 0.849d0, e_i=1.0d-4, Nf=64, ep = Neoi - 49.8d0 !?!?!
! A1 = 3.35d-14, A2 = 7.26d-15, del_phi = phi_0/7.5d3
!----------------------------------------------------------------------------------------------------------------------------------------------
! for Staro-II, against KDI-Hergt
! m = (4.4d-14)**(0.25), phi_0 = 0.5d0, phi_i = 2.0d0, e_i=1.0d-4, Nf=72, ep = Neoi - 50.2d0	!?!?!
! del_phi = phi_0/1.0d4, A1 = (1.0d-1)*V0, A2 = (5.0d-4)*V0					
!----------------------------------------------------------------------------------------------------------------------------------------------
!for Bhaumik et.al. 1907.04125
!## set 1#############################################
!phi_ini=coff(15)= 17.245000000000001 efold=coff(9)= 75.995003609568812 x0= 0.39800000000000002
!a,b,c,d,nu,V0=(coff(1-6)= 
! 8.5220045435404804E-002  -0.46899167535097835  1.0000000000000000  1.5092819918189926  4.3411702837156332   1.3252976145649607E-009
!a00=coff(8) =6.3999997906386859E-005

!## set 2 #############################################
!phi_ini=coff(15)= 17.245000000000001 efold=coff(9)= 75.993003609473817 x0= 0.38300000000000001
!a,b,c,d,nu,V0=(coff(1-6)= 
! 7.2450312039055734E-002  -0.43512722391552722  1.0000000000000000  1.5017550848670957  4.6380967246819518   1.5836288101998645E-009
!a00=coff(8) =6.3999997906386859E-005 
!----------------------------------------------------------------------------------------------------------------------------------------------
